# Initialize stuff when logging in on tty1
[ "$(tty)" = "/dev/tty1" ] && {

  # for conf in ~/.secrets/*.*sh; do 
  #   source $conf
  # done

  for conf in ~/.environment/*.*sh; do 
    source $conf
  done

  for conf in ~/.scripts/autorun/*.*sh; do 
    $conf & 
  done
  
  # for conf in ~/.scripts/wayland-autorun/*.*sh; do 
  #   $conf & 
  # done

  for conf in ~/.autorunloc/*.*sh; do 
    $conf & 
  done

  # exec sway
  # exec niri
}
