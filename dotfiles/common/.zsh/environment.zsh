export BROWSER="w3m"
export EDITOR="nvim"
export DIFFPROG="nvim -d"
export VISUAL="nvim"
export PAGER="less"
export MANPAGER="less -s -M +Gg"
export LC_COLLATE=C

export SESSION=$(loginctl show-session $(loginctl list-sessions | awk 'FNR==2{print $1}') -p Type | cut -d= -f2)
export DISTRO=$(cat /etc/os-release | awk -F '=' 'NR==3 {print $2}')
[[ -d /run/systemd/system ]] &&
  export INIT="systemd" ||
  export INIT="init"

# For smoother ctrl-backspace. Removed `-_/`
export WORDCHARS="*?[]~=&;!#$%^(){}<>"

# SSH Agent stuff
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"

# PATH stuff
# duplicates ~/.environment/path.sh, but needed on headless installs
export PATH="$PATH:$HOME/.cargo/bin"
export PATH="$PATH:$HOME/.local/bin"
export PATH="$PATH:$HOME/.scripts/path"

export TAWA_WILE="$HOME/.opt/tawa-wile/"

# Colors for less
export LESS_TERMCAP_md=$'\e[1;31m'
export LESS_TERMCAP_so=$'\e[1;33;100m'
export LESS_TERMCAP_us=$'\e[4;33m'
export LESS_TERMCAP_mb=$'\e[1;31m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_ue=$'\e[0m'
