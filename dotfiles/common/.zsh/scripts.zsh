# Re-enable compose key on X systems each time a terminal opens
# because sometimes it just dies on me
[[ $SESSION == "x11" ]] &&
  setxkbmap -option "compose:ralt"

# run ls every time pwd changes
# unless we ask it not to
ls_on_cd() {
  [[ -z ${DONT_LS:+x} ]] && {
    [[ $(which eza) ]] && eza || ls
    echo
  }
}
add-zsh-hook chpwd ls_on_cd
