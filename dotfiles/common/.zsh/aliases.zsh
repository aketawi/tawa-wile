# replacements, preferences and quick flags
alias ls='eza -s=Name'                                # make sure Uppercase names appear before lowercase
alias ll='eza -lhF -s=Name'                           # long, headers, type indicators (like @ for links)
alias la='eza -lhaF -s=Name'                          # list hidden
alias lt='eza -TF -L2 -s=Name'                        # tree, recurse 2 dirs max
alias tfe80::173e:6b65:27ea:6f17ree='eza -TF -s=Name' # tree, full recurse
alias lsd='eza -D -s=Name'                            # only list dirs

alias bat='bat --decorations never'

alias ps=procs
alias htop=btop
alias grep=rg
alias find=fd
alias mail='neomutt -e "set crypt_use_gpgme=no"'
alias dmenu="rofi -theme $XDG_CONFIG_HOME/rofi/theme.rasi -dmenu"

# atool pack/unpack
alias 'aup'='aunpack'
alias 'apu'='apack'

# systemctl and openrc
alias 'sy'='sudo systemctl'
alias 'sys'='sudo systemctl status'
alias 'syt'='sudo systemctl start'
alias 'syp'='sudo systemctl stop'
alias 'sye'='sudo systemctl enable'
alias 'syd'='sudo systemctl disable'

alias 'usy'='systemctl --user'
alias 'usys'='systemctl --user status'
alias 'usyt'='systemctl --user start'
alias 'usyp'='systemctl --user stop'
alias 'usye'='systemctl --user enable'
alias 'usyd'='systemctl --user disable'

alias 'syunits'='systemctl --no-pager --state=enabled list-unit-files; echo --Failed Units --; systemctl --failed'

alias 'jurn'='journalctl -eu'
alias 'jur'='journalctl -e'
alias 'ujurn'='journalctl --user -eu'
alias 'ujur'='journalctl --user -e'
alias 'jurboot'='journalctl -b0 -p crit --no-pager && echo; journalctl --disk-usage'

# navigation
alias mkdir='mkdir -p'
alias md=mkdir
alias rd=rmdir
# more navigation stuff in ./functions.zsh

# vim brainrot
alias ':q'=exit
alias ':qq'=exit
alias ':wq'=exit
alias ':w'=true # do nothing, succesfully
alias vi=nvim
alias vim=nvim
alias se=sudoedit

# git
alias gt='git status'
alias gf='git diff'
alias gfh='git fetch'
alias gcl='git clone --depth 1'
alias ga='git add'
alias ga.='git add .'
alias gg='git pull' # git get
alias gp='git push'
alias gpu='git push --set-upstream'
alias ggp='git pull && git push'
alias gc='git commit -v'
alias gct='git commit -vt commit.msg'
alias gcedit='git commit -c'
alias gcempty='git commit --allow-empty-message'
alias gcall='git commit -va'
alias gcam='git commit --amend'
alias gll='git log --graph --pretty=oneline --abbrev-commit'
alias gb='git branch'
alias gw='git switch'
alias gh='git checkout'
alias ghb='git checkout -b'
alias gmt='git mergetool'
alias gitui='gitui -t macchiato.ron' # theming
alias gitdeepclean='git -c gc.reflogExpire=0 -c gc.reflogExpireUnreachable=0 \
  -c gc.rerereresolved=0 -c gc.rerereunresolved=0 \
  -c gc.pruneExpire=now gc "$@"'

# pacman
alias yay=paru
alias yeet='paru -Rcns'
alias orphans='paru -Qqtd' # handy for: orphans | yeet -
alias cleen='paru -Scc'
alias mirror='sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist'
alias pacd='pacdiff -b -s'

# tmux
alias ta="tmux attach"

# clipboard
[[ $SESSION = "wayland" ]] &&
  alias cleep='wl-copy' ||
  alias cleep='xclip -r -selection clipboard'

# power control
[[ $INIT = "systemd" ]] && {
  alias shutdown="systemctl poweroff"
  alias reboot="systemctl reboot"
  alias restart="systemctl reboot"
  alias suspend="systemctl suspend-then-hibernate"
} || {
  alias shutdown="loginctl poweroff"
  alias reboot="loginctl reboot"
  alias restart="loginctl reboot"
  alias suspend="loginctl suspend-then-hibernate"
}

# docker
alias dco='docker compose'
alias dcup='docker compose up -d'
alias dcdown='docker compose down'
alias dcd=dcdown
alias dcb='docker compose build'
alias dcpull='docker compose pull'
alias dctail='docker compose logs -tf --tail="100"'

alias dtail='docker logs -tf --tail="100" "$@"'
alias dre='docker restart'
alias dswag='docker restart swag'
alias ldo='lazydocker'

# networking
alias getip='curl --silent ipinfo.io | jq ."ip" | tr -d \"'
alias oports='echo --Listening TCP Ports-- && ss -tlpn && echo -e "\n--Listening UDP Ports--" && ss -ulpn'
alias netroutes='echo -- Network Routes -- && ip -c route'
alias ip="ip -c"

# hardware
alias blk='udisksctl status; echo; echo -- BLK Devices --; lsblk'
alias cpu='echo -- CPU info --; lscpu |grep  "Model name"  && lscpu --extended --online'
alias fsls='echo -- FS info --; df -hT -x devtmpfs -x tmpfs -x efivarfs'
alias freem='echo -- Free MEM --; free -mt'

# files
alias dt="du * -csh 2>/dev/null | sort -h" # totals for dirs, sort by by size
alias ds="du -sh 2>/dev/null"              # total use of just pwd
alias fl='nnn -p -'
alias dnd='dragon-drop -x -T'
alias cx='chmod +x'
alias rt='trash'

# calendar
alias ik="ikhal"
alias ke="khal edit --show-past"
alias kc="khal calendar"

# python
alias py=python
alias ipy=ipython
alias vipy="vi $(fd .py)"

# other
alias termdim='echo $(stty size|cut -d" " -f1) rows x $(stty size|cut -d" " -f2) cols'
alias epoch='echo -e Seconds since Unix epoch: $(date +%s) && echo Seconds until Y2K38: $(( 2147483646 - $(date +%s) ))'
alias E='date +%s'
alias yd=yt-dlp
alias yf=ytfzf
alias yfpl="ytfzf --disable-submenus -m --type=playlist"
alias stop="kill -STOP"
alias cont="kill -CONT"
alias yoink="wget -mkEpnp -e robots=off"
alias gdl="gallery-dl"

# alphabet
alias g=git
alias o='xdg-open'
alias l=ls
alias q='qalc -c'
alias v='nvim .'
alias p='nvim ~/.plan'
# in functions.zsh:
# alias f=nnn
# alias t=tmux at directory
