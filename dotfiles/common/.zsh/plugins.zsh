# command suggestions
znap source zsh-users/zsh-autosuggestions
export ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#939ab7,underline"
export ZSH_AUTOSUGGEST_STRATEGY=(history completion)
export ZSH_AUTOSUGGEST_USE_ASYNC=true
export ZSH_AUTOSUGGEST_BUFFER_MAX_SIZE=30
export ZSH_AUTOSUGGEST_HISTORY_IGNORE="cd *"

# syntax highlight
# znap source zdharma-continuum/fast-syntax-highlighting
znap source zsh-users/zsh-syntax-highlighting
ZSH_HIGHLIGHT_HIGHLIGHTERS+=(brackets cursor regexp)

typeset -A ZSH_HIGHLIGHT_REGEXP
ZSH_HIGHLIGHT_REGEXP+=('\bsudo\b' fg=yellow,bold)
ZSH_HIGHLIGHT_REGEXP+=('\brm\b' fg=yellow,bold)

typeset -A ZSH_HIGHLIGHT_STYLES
ZSH_HIGHLIGHT_STYLES[alias]='fg=magenta'
ZSH_HIGHLIGHT_STYLES[variable]='fg=yellow'
ZSH_HIGHLIGHT_STYLES[path]='fg=cyan'
ZSH_HIGHLIGHT_STYLES[unknown-token]='bg=gray fg=yellow'
ZSH_HIGHLIGHT_DIRS_BLACKLIST+=(/files/)

# literally magic. fuzzy search for visited directories
znap source agkozak/zsh-z

# revamped Ctrl+R
znap source zdharma/history-search-multi-word
zstyle ":history-search-multi-word" highlight-color "fg=yellow,bold"

# additional completion definitions
znap install zsh-users/zsh-completions
