bindkey -e
typeset -g -A key

bindkey '\e[3~' delete-char        # Delete
bindkey '\e[3;5~' kill-word        # Ctrl+Delete
bindkey '^[t' kill-word            # Alt+T
bindkey '^H' backward-kill-word    # Ctrl+Backspace
bindkey '^[o' backward-kill-word   # Alt+O

bindkey '^t' end-of-line           # Ctrl+T (mirroring Ctrl+A for ^)
bindkey '^[[1;5C' forward-word     # Ctrl+Right
bindkey '^[w' forward-word         # Alt+W
bindkey '^[[1;5D' backward-word    # Ctrl+Left
bindkey '^[b' backward-word        # Alt+B

bindkey '^[e' down-line-or-search  # Alt+E
bindkey '^[i' up-line-or-search    # Alt+I

# satisfy my vim brainrot
exit_zsh() { exit }
zle -N exit_zsh
bindkey 'ZZ' exit_zsh

# Move up a directory
zsh_up() { builtin cd .. && zle reset-prompt }
zle -N zsh_up
bindkey '^[.' zsh_up               # Alt+.

# Move to previous directory
zsh_back() { builtin cd - && zle reset-prompt }
zle -N zsh_back
bindkey '^[-' zsh_back             # Alt+-

# Move home
zsh_cdhome() { builtin cd ~ && zle reset-prompt }
zle -N zsh_cdhome
bindkey '^[\`' zsh_cdhome           # Alt+~ (no shift)

# Tab for completion cycling
bindkey '\t' menu-select "$terminfo[kcbt]" menu-select
bindkey -M menuselect '\t' menu-complete "$terminfo[kcbt]" reverse-menu-complete

# Edit current line in $EDITOR     
autoload -U edit-command-line
zle -N edit-command-line
bindkey '^x^x' edit-command-line   # Ctrl+X+X

# Copy current line to clipboard   
to_clip() {
  if [[ $SESSION = "wayland" ]] ; then
    wl-copy <<< $BUFFER 
  else
    xclip -selection clipboard <<< $BUFFER
  fi
}
zle -N to_clip
bindkey '^x^v' to_clip             # Ctlr+X+V

# Insert last command result
zmodload -i zsh/parameter
insert-last() { LBUFFER+="$(eval $history[$((HISTCMD-1))])" }
zle -N insert-last
bindkey '^x^z' insert-last         # Ctrl+X+Z

# Insta sudo !!
insta-sudo() { LBUFFER+="sudo $history[$((HISTCMD-1))]" }
zle -N insta-sudo
bindkey '^[S' insta-sudo           # Alt+Shift+S

# Accept current suggestion
bindkey '^[a' autosuggest-accept   # Alt+A

# Execute current suggestion
bindkey '^[^M' autosuggest-execute # Alt+Return

# borderline black magic
# Press ^Z to move a task to or from background
fancy-ctrl-z() {
  if [[ $#BUFFER -eq 0 ]]; then 
    BUFFER="fg"
    zle accept-line -w
  else
    zle push-input -w
    zle clear-screen -w
  fi
}
zle -N fancy-ctrl-z
bindkey '^Z' fancy-ctrl-z
