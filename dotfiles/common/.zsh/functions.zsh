# remake `cd` to work seamlessly with zsh-z
# thanks to https://thenybble.de/posts/stupid-zsh-tricks
cd() { 
  # Go to home without arguments
  [ -z "$*" ] && builtin cd && return

  # If the directory exists wihtin pwd, normal cd
  [ -d "$*" ] && builtin cd "$*" && return
  [ "$*" = "-" ] && builtin cd "$*" && return

  # Catch . and ..
  case "$*" in 
    ..) builtin cd ..; return;;
    .) builtin cd .; return;;
  esac

  # Finally, z
  zshz "$*" || builtin cd "$*"
}

# quickly move into the dotfiles repo using z
# it's here and not in aliases because it relies on the function above
alias tw='cd opt/tawa-wile'

# edit zsh configs
erc() {
  prevdir=$(pwd)
  cd ~/.zsh && nvim .
  cd $prevdir
}

# make a dir and cd into it
take() {
  mkdir -p $1
  cd $1
}

# go up x directories
up() {
  cd $(printf "%0.s../" $(seq 1 $1));
}

# rsync reimplementation of cp
# shamelessly stolen from omz
rp() {
  rsync -pogbr -hhh --backup-dir="/tmp/rsync-${USERNAME}" -e /dev/null --progress "$@"
}

# list files to remove before removing them
# BUG: sends rm flags to ls, so fails to preview when using
rmi() {
  echo "Files to be deleted:"
  ls -l ${@: -1}
  echo
  echo "Do you wish to continue? [y/N] "
  read choice

  [[ $choice == "y" ]] &&
    rm $@ ||
    echo "Not removing..."
}

# run a program in background, silently
bgr() {
  nohup "$@" &>/dev/null 2>&1 &
}

# print out explicitly installed packages with description
pacls() {
  paru -Qqe | xargs -I{} -P0 --no-run-if-empty pacman -Qs "^{}\$";
  echo "Total packages installed: `paru -Qqe | wc -l`"
}

# browse AUR with fzf
alias fy="fzfyay"
fzfyay() { 
  paru -Slq | fzf -m --preview 'bat <(paru -Si {1}) <(paru -Fl {1} | awk "{print \$2}")' | xargs -ro paru -S
}

# bat-colored help
bh() {
  [[ -z "$@ --help" ]] || "$@" --help 2>&1 | bat --plain --language=help
}

# quickly create aliases in local folder
qal() {
  [[ $# -lt 2 ]] &&
    {
      echo "Usage: qal [alias] [full command] [optional comment]"
      return 1
    }

  if [[ ! -d "$HOME/.zshloc" ]]; then
    echo "~/.zshloc does not exist, creating..."
    echo "Make sure to source it in your .zshrc"
    mkdir "$HOME/.zshloc"
  fi

  if [[ -n $3 ]]; then
    echo "# $3" >> "$HOME/.zshloc/aliases.zsh"
  else
    echo "# alias of '$1' to '$2'" >> "$HOME/.zshloc/aliases.zsh"
  fi

  echo "alias '$1'='$2'" >> "$HOME/.zshloc/aliases.zsh"
  source "$HOME/.zshloc/aliases.zsh"

  echo "Alias added."
}

# internal hacky subroutine to make sure we know its pid
# called by sloop()
_real_sloop() {
  sleep $1 && {

    # break execution if removed .sloop and killed sleep with unsloop
    if [[ -e ~/.sloop ]] || return

    rm ~/.sloop
    which playerctl >/dev/null 2>&1 && playerctl pause
    [[ $INIT = systemd ]] && systemctl suspend
    [[ $INIT = init ]] && loginctl suspend-then-hibernate
  }
}

# suspend after an amount of time
sloop() {
  [[ -z $SSH_CLIENT ]] || {
    echo "you're in ssh. proceed? [y/N]"; read -sk1 choice
    [[ $choice == "y" ]] || return
  }

  if [[ -e ~/.sloop ]] ; then
    echo "sloop already running at pid $(\cat ~/.sloop)"
    echo "kill using unsloop to set a new time"
    return
  fi

  # default to instant sloop if no argument provided
  timeout=$1
  [[ -z $timeout ]] && timeout=0

  # hacky workaround cuz we need to store the pid
  _real_sloop $timeout &
  echo $! > ~/.sloop
}

# kill sloop process if you changed your mind
unsloop() {
  if [[ ! -e ~/.sloop ]] ; then
    echo "not slooping"
    return
  fi

  kill $(\cat ~/.sloop)
  rm ~/.sloop
}

# instantly push all changes to the repo
twa() {
  DONT_LS=true
  prevdir=$(pwd)
  zshz opt/tawa-wile 
  git add .
  git commit -a
  ! git push
  [[ $? -ne 0 ]] || echo "NOTICE: Push failed, probably because repo is diverging with origin."
  cd $prevdir
  unset DONT_LS
}

# search wikipedia for a query
wikidig() {
  dig +short txt $1.wp.dg.cx | rev | cut -d "|" -f 1 | rev
}

# use zsh-z to navigate to a directory and open a tmux session with the same name
# inspired by https://github.com/joshmedeski/t-smart-tmux-session-manager
t() {
  [[ $# == 0 ]] &&  # if no arguments
    {
      echo "Usage: t [name of session or approximate name of a directory]"
      return  
    }

  tmux has -t "$1" >/dev/null 2>&1
  [[ $# -lt 1 ]] && tmux swtichc -t "$1"

  prevdir=$(pwd)           # save current dir to go back to
  DONT_LS=yes cd $1        # try to z-cd into given directory
  [[ $? == 0 ]] || return  # if error, return

  session_name=$(pwd | sed 's/.*\///g') # get last directory in pwd

  # if running outside of tmux
  [[ -z $TMUX ]] &&
    {
      tmux new -As $session_name -c $(pwd)
      cd $prevdir
      return
    }

  # if session exists just switch
  tmux has -t="$session_name" 2> /dev/null &&  
    {
      DONT_LS=yes builtin cd $prevdir
      tmux switchc -t $session_name
      return
    }

  # else create new session at directory and switch
  # not using -A to prevent nesting
  tmux_session=$(tmux new -Pds $session_name -c $(pwd))
  DONT_LS=yes builtin cd $prevdir
  tmux switchc -t $tmux_session 
}

# convert a symlink to a local file
# https://overflow.adminforge.de/questions/8377312/how-to-convert-symlink-to-regular-file
getreal() {
  [[ -L "$1" ]] && cp --remove-destination "$(readlink "$1")" "$1"
}

# back up a file
bak() {
  cp $1 $1.bak
}

# find and open files in editor
fe() {
  [[ -z $1 ]] && {
    echo "no arguments, refusing to open literally every file on the filesystem..."
    return 1
  }

  results="$(fd $1 | sed 's/.*/"&"/' | tr '\n' ' ')"  # wrap in quotes and strip newlines
  [[ -z "$results" ]] && {
    echo "no matches found..."
    return 1
  }

  sh -c "$EDITOR $results"
}

# turn terminal transparency on and off
# hint: run `getreal` on alacritty.yml if you plan to use this often.
transon() {
  [[ -z $SSH_CLIENT ]] || { echo "you're in ssh dumbass"; return }
  { [[ -f ~/.config/alacritty/alacritty.toml ]] &&
    sed -i 's/opacity.*/opacity = 0.78/' ~/.config/alacritty/alacritty.toml } ||
  { [[ -f ~/.config/alacritty/alacritty.yml ]] &&
    sed -i 's/opacity.*/opacity: 0.78/' ~/.config/alacritty/alacritty.yml }
}

transet() {
  [[ -z $SSH_CLIENT ]] || { echo "you're in ssh dumbass"; return }
  { [[ -f ~/.config/alacritty/alacritty.toml ]] &&
    sed -i "s/opacity.*/opacity = $1/" ~/.config/alacritty/alacritty.toml } ||
  { [[ -f ~/.config/alacritty/alacritty.yml ]] &&
    sed -i "s/opacity.*/opacity: $1/" ~/.config/alacritty/alacritty.yml }
}

transoff() {
  [[ -z $SSH_CLIENT ]] || { echo "you're in ssh dumbass"; return }
  { [[ -f ~/.config/alacritty/alacritty.toml ]] &&
    sed -i 's/opacity.*/opacity = 1/' ~/.config/alacritty/alacritty.toml } ||
  { [[ -f ~/.config/alacritty/alacritty.yml ]] &&
    sed -i 's/opacity.*/opacity: 1/' ~/.config/alacritty/alacritty.yml }
}


# same thing for picom's blur
bluron() {
  [[ -z $SSH_CLIENT ]] || { echo "you're in ssh dumbass"; return }
  [[ -f ~/.config/picom.conf ]] &&
    sed -i 's/method.*/method = "dual_kawase";/' ~/.config/picom.conf
}

bluroff() {
  [[ -z $SSH_CLIENT ]] || { echo "you're in ssh dumbass"; return }
  [[ -f ~/.config/picom.conf ]] &&
    sed -i 's/method.*/method = "none";/' ~/.config/picom.conf
}

blurset() {
  [[ -z $SSH_CLIENT ]] || { echo "you're in ssh dumbass"; return }
  [[ -f ~/.config/picom.conf ]] &&
    sed -i "s/strength.*/strength = $1;/" ~/.config/picom.conf
}

# mask a cronjob by partial matching
# does root crontab with -s
# mask everything with -a
# this whole monstrocity is here just because I wanted to
# listen to prismrivers' theme while jobbing into the night and was afraid
# that my media server would go offline at night as scheduled and thought
# hey wouldn't it be nice if i could tell the server to stop going
# to sleep at set time of day with just one command instead of manually
# commenting out the cronjob everytime?
# it is now almost 2:30 am and i've been listening to it on repeat while
# making sure this actually works as intended
# best theme in pcb.
cronmask() {
  while getopts "as" option; do
    case $option in
      a) ALL_ALLOWED=true 
         export ALL_ALLOWED
         [[ $UID -eq 0 ]] ||  # suppress if rerunning as root
           echo "NOTICE: Operating on every cronjob..." 
         ;;
      s) [[ $UID -eq 0 ]] ||  # re run as root. hacky, but a decent way
          {                   # to pass the function contents to root
            args=$@                                                       
            sudo bash -c "$(declare -f cronmask); cronmask $args"    
            return                                                 
          } ;;
    esac
  done 

  shift $((OPTIND - 1))  # yeet flags
  if [[ -z $@ ]] && [[ $ALL_ALLOWED != true ]]; then 
    echo "-a not passed, refusing to operate on every entry..."
    unset ALL_ALLOWED
    return 1
  fi
  
  LOCATION="/var/spool/cron/$USER"
  awk "/$1/ && !/#/" "$LOCATION" | while read line; do
    printf -v sedcmd 's|%q|%q|' "$line" "# MASKED: $line"

    cp -f "$LOCATION" /tmp/cronsed  # workaround for sed's
    sed -i  "$sedcmd" "/tmp/cronsed"
    cp -f /tmp/cronsed "$LOCATION"
    rm -f /tmp/cronsed || true

    unset ALL_ALLOWED
    echo "Masked \"$line\"."
  done
}

# unmask a masked cronjob
cronumask() {
  while getopts "as" option; do
    case $option in
      a) ALL_ALLOWED=true 
         export ALL_ALLOWED
         [[ $UID -eq 0 ]] ||  # suppress if rerunning as root
           echo "NOTICE: Operating on every cronjob..." 
         ;;
      s) [[ $UID -eq 0 ]] ||
          {                 
            args=$@                                                       
            sudo bash -c "$(declare -f cronumask); cronumask $args"    
            return                                                 
          } ;;
    esac
  done

  shift $((OPTIND - 1))  # yeet flags
  if [[ $@ -lt 1 ]] && [[ $ALL_ALLOWED != true ]]; then 
    echo "-a not passed, refusing to operate on every entry..."
    unset ALL_ALLOWED
    return 1
  fi

  LOCATION="/var/spool/cron/$USER"
  awk "/$1/ && /# MASKED:/" "$LOCATION" | while read line; do
    cp "$LOCATION" /tmp/cronsed
    sed -i "s|\# MASKED\: *||" "/tmp/cronsed"
    cp -f /tmp/cronsed "$LOCATION" 
    rm -f /tmp/cronsed || true

    unset ALL_ALLOWED
    echo "Unmasked \"$line\"."
  done
}

# turns a string into QR via qrenco
qrize() {
  query=$(sed 's/ /%20/g' <<< "$1")
  curl -m5 qrenco.de/$query
}

# W3M functions 
websearch() {
  query=$(sed 's/ / /g' <<< "$@" | tr -d '\n')
  w3m "https://librey.garudalinux.org/search.php?q=$query"
}

# Create and/or source a python environment
penv() {
  [[ -d ./.venv/ ]] || python -m venv .venv
  source ./.venv/bin/activate
}

# nnn shortcut with cd on exit
f ()
{
    # Block nesting of nnn in subshells
    [ "${NNNLVL:-0}" -eq 0 ] || {
        echo "nnn is already running"
        return
    }

    export NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"

    command nnn "$@"

    [ ! -f "$NNN_TMPFILE" ] || {
        . "$NNN_TMPFILE"
        rm -f "$NNN_TMPFILE" > /dev/null
    }
}

# stop tracking a file in git locally
guntrack() {
  [[ -z $1 ]] && {
  echo "Usage: guntrack <name of file in a repo or symlink>"
  return 1
}

  d=$(dirname $(realpath "$1")) || return 1
  f=$(basename $(realpath "$1")) || return 1

  [[ -z $d ]] && {
    echo "Couldn't extract directory. Is your path correct?"
    return 2
  }

  [[ -z $f ]] && {
    echo "Couldn't extract filename. Is your path correct?"
    return 3
  }

  [[ ! -f "$d/$f" ]] && {
    echo "File $f does not exist."
    return 4
  }

  DONT_LS=true 
  prevdir=$(pwd)

  \cd "$d"
  git update-index --assume-unchanged "$f"

  \cd $prevdir
  unset DONT_LS

  echo "'$d/$f' removed from git tracking." 
}

P() {
  proxychains $(whence $1) ${@[@]:2}
}

noise() {
  [[ -z $1 ]] && return 1
  case $1 in
    "err") paplay --volume=35000 "${TAWA_WILE}/noises/cancel.wav" ;;
    "oki") paplay --volume=35000 "${TAWA_WILE}/noises/decision.wav" ;;
    "wrn") paplay --volume=35000 "${TAWA_WILE}/noises/get_item.wav" ;;
    "inp") paplay --volume=35000 "${TAWA_WILE}/noises/instruction.wav" ;;
  esac
}

logme() {
  set -e
  time=$(sed -r "s/(..)(.*)/\1:\2/" <<<"$1")
  set +e
  [[ $# -ge 3 ]] && {
    if [[ $2 == \+* ]]; then 
      to=$(tr -d "+" <<<"$2")
    else
      set -e
      to=$(sed -r "s/(..)(.*)/\1:\2/" <<<"$2")
      set +e
    fi
    name="$3"
    khal new "$time" "$to" "$name" -m 15m
    return 0
  }

  [[ $# -ge 2 ]] && {
    name="$2"
    if [[ $1 == \+* ]]; then 
      to=$(tr -d "+" <<<"$1")
      khal new $(date +"%R") "$to" "$name"
    else
      khal new "$time" $(date +"%R") "$name"
    fi
    return 0
  }

  [[ $# -ge 1 ]] && {
    name="$1"
    khal new $(date +"%H:00") 30m "$name"
    return 0
  }

  [[ $# -ge 0 ]] && {
    if [[ -n $(khal at) ]]; then
      echo "Now:"
      khal at
    else
      echo "No current task."
    fi
    khal list 
    return 0
  }
}
