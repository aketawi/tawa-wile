set -g @plugin 'tmux-plugins/tpm'             # the plugin manager

set -g @plugin 'tmux-plugins/tmux-sensible'   # some basic configs

set -g @plugin 'catppuccin/tmux'              # catppuccin styled status line
  set -g @catppuccin_flavour 'macchiato'

  set -g @catppuccin_status_fill "all"
  set -g @catppuccin_status_modules_right "directory host session"
  set -g @catppuccin_status_left_separator  " "
  set -g @catppuccin_status_right_separator ""
  set -g @catppuccin_status_connect_separator "yes"

  set -g @catppuccin_host_color "pink"
  set -g @catppuccin_directory_color "yellow"

  set -g @catppuccin_window_left_separator  "█"
  set -g @catppuccin_window_right_separator "█"

  set -g @catppuccin_window_default_text "#W" # running program name
  set -g @catppuccin_window_default_fill "number"

  set -g @catppuccin_window_current_text "#W"
  set -g @catppuccin_window_current_fill "number"
  set -g @catppuccin_window_tabs_enabled on

set -g @plugin 'tmux-plugins/tmux-yank'       # yanking in visual mode

set -g @plugin 'tmux-plugins/tmux-open'       # run open on selection
  set -g @open-S 'https://www.duckduckgo.com/?q='
  set -g @open 'h'

set -g @plugin 'Morantron/tmux-fingers'       # vimium-esque highlights and yanks
  set -g @fingers-key Space                   # tmux-thumbs -esque
  set -g @fingers-hint-style "fg=yellow,bold"
  set -g @fingers-backdrop-style "fg=white,dim"
  set -g @fingers-keyboard-layout "colemak"
  set -g @fingers-pattern-0 '"(.*?)"'         # yank inside double quotes
  set -g @fingers-pattern-1 "'(.*?)'"         # single quotes

set -g @plugin 'tmux-plugins/tmux-resurrect'  # store and restore tmux sessions

# set -g @plugin 'tmux-plugins/tmux-continuum'  # resurrect sessions automatically
#   set -g @continuum-restore 'on'

set -g @plugin 'jaclu/tmux-mouse-swipe'       # mouse gestures

set -g @plugin 'jaclu/tmux-menus'             # comfy menu at prefix-\

set -g @plugin 'roosta/tmux-fuzzback'         # fuzzy search through scrollback
  set -g @fuzzback-bind 'o'
  set -g @fuzzback-hide-preview 1

set -g @plugin 'laktak/extrakto'              # powerful string extractor from current buffer
  set -g @extrakto_key "r"

set -g @plugin 'omerxx/tmux-sessionx'         # comfy replacement session manager 
  set -g @sessionx-bind 'M-s'                 # bring up with M-s, no prefix
  set -g @sessionx-prefix off
  set -g @sessionx-window-height '100%'
  set -g @sessionx-window-width '100%'

set -g @plugin 'pschmitt/tmux-ssh-split'      # retain ssh session in splits
  set -g @ssh-split-v-key 'M-u'
  set -g @ssh-split-h-key 'M-y'
  set -g @ssh-split-keep-cwd true

# vim: ft=tmux
