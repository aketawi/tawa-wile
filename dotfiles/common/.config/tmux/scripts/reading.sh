#!/bin/bash
# expect a single focused pane in the session
# change alacritty font size and create two panes
# on either end acting as spacers

SOCKET="$(tmux showenv | grep SOCKET | cut -d= -f2)"

if [[ -z ${READING_MODE:+x} ]]; then
  tmux splitp -h
  tmux resizep -x 40
  tmux selectp -L
  tmux splitp -h
  tmux swapp -U
  tmux resizep -x 40
  tmux selectp -R
  alacritty msg -s "$SOCKET" config font.size=15

  tmux setenv READING_MODE true
else
  tmux selectp -R
  tmux killp
  tmux selectp -L
  tmux killp
  alacritty msg -s "$SOCKET" config font.size=11

  tmux setenv -u READING_MODE
fi

true # ignore bad exit codes
