#!/bin/bash
# detach if on scratchpad, attach if not
# so that you can use the same key to toggle it

[[ $1 = "-s" ]] && tmux kill-session -t scratch

if [[ "$(tmux display -p '#{session_name}')" = "scratch" ]]; then
  tmux detach
else
  dir="$(tmux display -p "#{pane_current_path}")"
  tmux popup -Ey100% -h40% -w100% "tmux new -ADs scratch -c \"$dir\""
fi

true  # ignore bad exit codes
