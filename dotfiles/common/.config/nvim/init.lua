-- following https://github.com/LunarVim/Neovim-from-scratch
-- and https://martinlwx.github.io/en/config-neovim-from-scratch/
--
-- Most of the configuration assumes you're using Colemak

require "options"
require "autocommands"
require "keybinds"
require "lazyloader"
require "configs"
