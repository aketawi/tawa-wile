-- Functional wripper for mapping custom keybindings
local function map(mode, lhs, rhs)
  local options = { noremap = true, silent = true }
  if opts then
    options = vim.tbl_extend("force", options)
  end
  vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end

-- More keybinds in plugins/whichkey.lua

--Remap space as leader key
map("", "<Space>", "<Nop>")
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Colemak rebinds: hjkl>neio
map("n", "n", "h")  -- move Left
map("n", "e", "gj") -- move Down (g to allow move within wrapped lines)
map("n", "i", "gk") -- move Up
map("n", "o", "l")  -- move Right

map("n", "l", "u")  -- Undo, like on qwerty

map("n", "u", "i") -- Insert, like on qwerty
map("n", "U", "I")

map("v", "u", "i") -- Insert and "in"
map("v", "U", "I")

map("n", "h", "o") -- Newline insert
map("n", "H", "O")

map("n", "E", "e") -- end of word        replaces (e)nd, because who ever uses 'end of WORD'
map("n", "k", "n") -- next/prev match
map("n", "K", "N")

-- Visual Colemak
map("v", "n", "h")  -- move Left
map("v", "e", "gj") -- move Down
map("v", "i", "gk") -- move Up
map("v", "o", "l")  -- move Right

map("v", "E", "e")  -- end of word        replaces (e)nd, because who ever uses 'end of WORD'
map("n", "k", "n")  -- next/prev match
map("n", "K", "N")

-- Ergonomic exits
map("i", "<A-/>", "<esc>") -- the slash on my laptop is in the same position
map("i", "<A-.>", "<esc>") -- as the dot on my desktop keyboard
map("v", "<A-/>", "<esc>")
map("v", "<A-.>", "<esc>")
map("n", "<A-/>", "<esc>") -- Just so that it doesn't jump to search in normal
map("n", "<A-.>", "<esc>")

-- Window navigation with tmux integration
-- Somehow, still works with nerdtree.
map("n", "<C-n>", "<cmd> TmuxNavigateLeft<CR>")
map("n", "<C-e>", "<cmd> TmuxNavigateDown<CR>")
map("n", "<C-i>", "<cmd> TmuxNavigateUp<CR>")
map("n", "<C-o>", "<cmd> TmuxNavigateRight<CR>")

-- Buffer navigation
map("n", "O", ":bnext<CR>")
map("n", "N", ":bprevious<CR>")

-- Stay in visual after indent
map("v", "<", "<gv")
map("v", ">", ">gv")

-- Move lines
map("n", "<A-e>", ":m .+1<CR>==")
map("n", "<A-i>", ":m .-2<CR>==")
map("v", "<A-e>", ":m .+1<CR>==")
map("v", "<A-i>", ":m .-2<CR>==")

-- Replace in visual
map("v", "p", '"_dP')

-- Move text in visual Block
map("x", "<A-e>", ":move '>+1<CR>gv-gv")
map("x", "<A-i>", ":move '<-2<CR>gv-gv")

-- Easier quit
map("c", "qq", "q!")
map("c", "qa", "qa!")

-- Remove search highlights and notifications on esc
map("n", "<esc>", ":nohl<CR>:lua require('noice').cmd('dismiss')<CR>")

-- quicker follow links
map("n", "L", "gx")
map("v", "L", "gx")

-- don't jump to next value with *
map("n", "*", "*N")
map("v", "*", "*N")
