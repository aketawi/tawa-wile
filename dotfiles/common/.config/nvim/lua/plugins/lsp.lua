local servers = {
  "lua_ls",
  "basedpyright", -- strict typing and lsp
  "rust_analyzer",
  "cssls",
  "html",
  "bashls",

  -- configured in ./null-ls.lua
  -- but need to be installed in mason:
  -- "ansiblelint",
  -- "shellcheck",
  --
  -- "black",
  -- "prettierd",
  -- "shfmt",
}

lsp_formatting = function(bufnr)
  vim.lsp.buf.format({
    filter = function(client)
      local skip = { "html", "cssls" } -- LSPs to skip formatting for
      for i = 1, #skip do
        if (skip[i] == client.name) then
          return false
        end
      end

      return true
    end,
    async = true,
    bufnr = bufnr,
  })
end

local lsp_config = function()
  -- This is where all the LSP shenanigans will live
  local lsp_zero = require('lsp-zero')
  local lsp = require('lspconfig')
  lsp_zero.extend_lspconfig()

  -- see :help lsp-zero-keybindings to learn the available actions
  lsp_zero.on_attach(function(client, bufnr)
    lsp_zero.default_keymaps({ buffer = bufnr })
  end)

  lsp_zero.set_sign_icons({
    error = '✘',
    warn = '▲',
    hint = '⚑',
    info = '»'
  })

  require('mason-lspconfig').setup({
    ensure_installed = servers,
    lsp_zero.configure('gdscript', {
      force_setup = true,                      -- because the LSP is global. Read more on lsp-zero docs about this.
      single_file_support = false,
      root_dir = require('lspconfig.util').root_pattern('project.godot', '.git'),
      filetypes = { 'gd', 'gdscript', 'gdscript3' }
    }),

    handlers = {
      lsp_zero.default_setup,
      -- Add server handlers here

      html = function()
        lsp.html.setup({
          filetypes = { "html", "htmldjango" }
        })
      end,

      bashls = function()
        lsp.bashls.setup({
          filetypes = { "bash", "sh" }
        })
      end,

      basedpyright = function()
        lsp.basedpyright.setup({
          settings = { basedpyright = { analysis = { typeCheckingMode = "strict" } } },
        })
      end,

      lua_ls = function()
        local lua_opts = lsp_zero.nvim_lua_ls()
        lsp.lua_ls.setup(lua_opts)
      end,

      pylsp = function()
        lsp.pylsp.setup({
          settings = {
            pylsp = {
              configurationSources = { "flake8" },
              plugins = {
                flake8 = {
                  enabled = true,
                  maxLineLength = 88, -- consistent with Black
                  maxComplexity = 10,
                  ignore = { "W503", "W504", "E203", "E704", "C901" },
                },
                pydocstyle = {
                  enabled = false,
                  convention = "google",
                  addIgnore = { "D103", "D202", "D415" }
                },

                -- using with Black and Pyright
                -- so we only want formatting hints, not full LSP
                jedi_completion = { enabled = false },
                jedi_hover = { enabled = false, },
                jedi_references = { enabled = false, },
                jedi_signature_help = { enabled = false, },
                jedi_symbols = { enabled = false, },
                pylint = { enabled = false },
                pyflakes = { enabled = false },
                preload = { enabled = false },
                pycodestyle = { enabled = false, },
                autopep8 = { enabled = false },
                yapf = { enabled = false },
              },
            }
          },
        })
      end,

      rust_analyzer = function()
        lsp.rust_analyzer.setup({
          cargo = {
            allFeatures = true,
            loadOutDirsFromCheck = true,
            runBuildScrigts = true,
          },

          checkOnSave = {
            allFeatures = true,
            command = "clippy",
            -- extraArgs = { "--no-deps" }
          },

          check = {
            features = "all" -- checkOnSave.allFeatures doesn't seem to work..?
          },

          procMacro = {
            enable = true,
            ignored = {
              ["async-trait"] = { 'async_trait' },
              ["napi-derive"] = { 'napi' },
              ["async-recursion"] = { 'async_recursion' },
            },
          },

          inlayHints = {
            enable = true,
          },
        })
      end,

      lsp_zero.setup_servers({ servers })
    }
  })
end


local autocompletion_config = function()
  local lsp_zero = require('lsp-zero')
  lsp_zero.extend_cmp()

  local cmp = require('cmp')
  local cmp_action = lsp_zero.cmp_action()
  local luasnip = require("luasnip")
  local select_opts = { behavior = cmp.SelectBehavior.Select }

  require("luasnip.loaders.from_vscode").lazy_load()

  cmp.setup({
    preselect = 'item',
    completion = {
      completeopt = 'menu,menuone,noinsert'
    },

    snippet = {
      expand = function(args)
        luasnip.lsp_expand(args.body)
      end,
    },

    window = {
      completion = cmp.config.window.bordered(),
      documentation = cmp.config.window.bordered(),
    },

    sources = {
      { name = "nvim_lsp" },
      { name = "path",       keyword_length = 2 },
      { name = "luasnip",    keyword_length = 2 },
      { name = "treesitter", keyword_length = 3 },
      { name = "calc",       keyword_length = 3 },
      { name = "obsidian",   keyword_length = 3 },
    },

    formatting = {                         -- how completions will appear
      fields = { "menu", "abbr", "kind" }, -- icon, entry name, its type
      format = function(entry, item)       -- format entries with icons
        local menu_icon = {                -- set icons
          nvim_lsp = "λ",
          luasnip = "",
          cmdline = ":",
          buffer = "󰏉",
          path = "",
          treesitter = "󰙅",
        }
        item.menu = menu_icon[entry.source.name] -- use the icon
        return item
      end,
    },

    mapping = cmp.mapping.preset.insert({
      ['<C-Space>'] = cmp.mapping.complete(),
      ['<S-Up>'] = cmp.mapping.scroll_docs(-4),
      ['<S-Down>'] = cmp.mapping.scroll_docs(4),

      -- confirm and abort
      ["<A-a>"] = cmp.mapping.confirm({ select = true }),
      ["<CR>"] = cmp.mapping.confirm({ select = false }),
      ["<A-t>"] = cmp.mapping({
        i = cmp.mapping.abort(),
        c = cmp.mapping.close(),
      }),

      ['<Tab>'] = cmp_action.luasnip_supertab(),
      ['<S-Tab>'] = cmp_action.luasnip_shift_supertab(),
    }),
  })

  cmp.setup.cmdline(":", {
    mapping = cmp.mapping.preset.cmdline(),
    sources = cmp.config.sources({ { name = "path" } }, {
      {
        name = "cmdline",
        option = {
          ignore_cmds = { "Man", "!" },
        },
      },
    }),
  })
  luasnip.filetype_extend("python", { "pydoc" })
  luasnip.filetype_extend("htmldjango", { "html" })
  luasnip.filetype_extend("zsh", { "sh", "shelldoc" })
  luasnip.filetype_extend("sh", { "shelldoc" })
end

-----------------------------------------------------------------------------------------
return {
  { -- boilerplates for LSPs
    'VonHeikemen/lsp-zero.nvim',
    branch = 'v3.x',
    lazy = true,
    config = false,
    init = function()
      vim.g.lsp_zero_extend_cmp = 0
      vim.g.lsp_zero_extend_lspconfig = 0
    end,
  },

  ---------------------------------------------------------------------------------------
  { -- LSP installer
    "williamboman/mason.nvim",
    lazy = false,
    config = true,
    opts = {
      log_level = vim.log.levels.INFO,
      max_concurrent_installers = 4,
      ui = {
        icons = {
          package_installed = "✓",
          package_pending = "➜",
          package_uninstalled = "✗"
        },
        border = "rounded"
      },
    }
  },

  ---------------------------------------------------------------------------------------
  { -- autocompletion
    'hrsh7th/nvim-cmp',
    event = 'InsertEnter',
    dependencies = {
      'L3MON4D3/LuaSnip',
      "saadparwaiz1/cmp_luasnip",     -- completion engine
      "hrsh7th/cmp-nvim-lsp",         -- integration with language servers
      "hrsh7th/cmp-buffer",           -- complete words from current buffer
      "hrsh7th/cmp-path",             -- complete file system paths (/home/...)
      "hrsh7th/cmp-cmdline",          -- completions for command mode
      "rafamadriz/friendly-snippets", -- snippets
      "hrsh7th/cmp-calc",             -- calculate expressions
      "ray-x/cmp-treesitter",         -- complete from treesitter nodes
    },
    config = autocompletion_config,
  },

  ---------------------------------------------------------------------------------------
  { -- LSPconfig
    'neovim/nvim-lspconfig',
    cmd = 'LspInfo',
    event = { 'BufReadPre', 'BufNewFile' },
    dependencies = {
      'hrsh7th/cmp-nvim-lsp',
      'williamboman/mason-lspconfig.nvim',
    },
    config = lsp_config,
  },
}
