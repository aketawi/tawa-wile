-- quickly comment out blocks of code
return {
  "numToStr/Comment.nvim",
  name = "Comment",
  opts = {
    toggler = {
      line = "gcc",  -- Line-comment toggle keymap
      block = "gbc", -- Block-comment toggle keymap
    },

    opleader = {
      line = "gc",  -- Line-comment keymap
      block = "gb", -- Block-comment keymap
    },

    extra = {
      eol = "gcA", -- add a comment at eol
    },
  },
}
