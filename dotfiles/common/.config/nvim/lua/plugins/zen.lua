return {
  {
    "folke/twilight.nvim",
    opts = {
      dimming = {
        alpha = .5,
      },
      context = 20,
    }
  },
  {
    "folke/zen-mode.nvim",
    opts = {
      window = {
        backdrop = 1,
        options = {
          signcolumn = "no",      -- disable signcolumn
          number = false,         -- disable number column
          relativenumber = false, -- disable relative numbers
          cursorline = true,      -- enable cursorline
          foldcolumn = "0",       -- disable fold column
          list = false,           -- disable whitespace characters
        },
      },
      plugins = {
        ruler = true,
        gitsigns = true,
        tmux = true,
      }
    }
  }
}
