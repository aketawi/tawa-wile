-- status bar below
return {
  'nvim-lualine/lualine.nvim',
  config = function()
    local lualine = require("lualine")
    lualine.setup({
      options = {
        icons_enabled = true,
        theme = "catppuccin",
        disabled_filetypes = { "dashboard", "NvimTree", "Outline" },
        always_divide_middle = true,
      },
      sections = {
        lualine_x = { 'encoding', 'filetype' }
      }
    })

    -- lualine.hide()         -- hide by default, unhide with Lr-uh
    vim.opt.laststatus = 0 -- hide status line, which will be handled by Lualine
  end
}
