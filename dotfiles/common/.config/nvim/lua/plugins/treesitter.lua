-- syntax highlight, LSP hints, other magic
return {
  "nvim-treesitter/nvim-treesitter",
  dependencies = {
    "nvim-treesitter/nvim-treesitter-textobjects",
    "windwp/nvim-ts-autotag",
    'nvim-treesitter/playground',
  },
  config = function()
    local configs = require("nvim-treesitter.configs")

    configs.setup {
      ensure_installed = {
        "python",
        "rust",
        "vim",
        "vimdoc",
        "query",
        "tsx",
        "lua",
        "bash",
        "regex",
      },

      auto_install = true,
      sync_install = false,
      indent = { enable = true, disable = { "yaml" } },

      highlight = {
        enable = true,    -- false will disable the whole extension
        disable = { "" }, -- list of languages that will be disabled
        additional_vim_regex_highlighting = true,
      },


      incremental_selection = {
        enable = true,
        keymaps = {
          init_selection    = "<Leader>ts",
          node_incremental  = "<Leader>i",
          node_decremental  = "<Leader>e",
          scope_incremental = "<Leader>I",
        },
      },

      textobjects = {
        select = {
          enable = true,
          lookahead = true,
          keymaps = {
            ["af"] = "@function.outer",
            ["uf"] = "@function.inner",
            ["ac"] = "@class.outer",
            ["uc"] = "@class.inner",
            ["ai"] = "@conditional.outer",
            ["ui"] = "@conditional.inner",
          },
        },
      },

      autotag = {
        enable = true,
        enable_rename = true,
        enable_close = true,
        enable_close_on_slash = true,
        filetypes = {
          'html', 'htmldjango', 'javascript', 'typescript', 'javascriptreact',
          'typescriptreact', 'svelte', 'vue', 'tsx', 'jsx',
          'rescript', 'xml', 'php', 'markdown', 'astro',
          'glimmer', 'handlebars', 'hbs'
        }
      },

      playground = {
        enable = true,
      },
    }
  end
}
