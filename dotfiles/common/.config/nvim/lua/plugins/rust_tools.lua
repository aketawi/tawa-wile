-- rust LSP, formatting, hints, etc
return {
  'simrat39/rust-tools.nvim',
  dependencies = { "nvim-lua/plenary.nvim" },
  event = { "BufReadPost *.rs" },
  config = function()
    local rt = require("rust-tools")
    rt.setup({
      server = {
        on_attach = function(_, bufnr)
          -- Hover actions
          vim.keymap.set("n", "<C-space>", rt.hover_actions.hover_actions, { buffer = bufnr })
        end,
      },
      tools = {
        reload_workspace_from_cargo_toml = true,
        inlay_hints = {
          max_len_align = false,
        },
        lens = {
          enable = true,
        },
        checkonsave = {
          command = "clippy",
        },
        procMacros = {
          enabled = true,
        },
      },
    })
  end
}
