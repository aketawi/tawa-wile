-- hop to any word on screen
return {
  "phaazon/hop.nvim",
  opts = {
    keys = 'arstneiodhwfuyxcvkm'
  }
}
