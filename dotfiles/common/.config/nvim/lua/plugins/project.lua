-- some telescope features for whole projects
return {
  "ahmedkhalf/project.nvim",
  config = function()
    require("project_nvim").setup {}
  end
}
