return {
  "epwalsh/obsidian.nvim",
  dependencies = {
    "nvim-lua/plenary.nvim",
  },
  lazy = true,
  ft = "markdown",
  event = {
    "BufReadPre " .. vim.fn.expand "~" .. "/tawi-obsidian/tawa-wile/**.md",
    "BufNewFile " .. vim.fn.expand "~" .. "/tawi-obsidian/tawa-wile/**.md",
  },
  config = function()
    require("obsidian").setup({

      open_notes_in = "vsplit",
      disable_frontmatter = true,
      new_notes_location = "current_dir",

      workspaces = {
        {
          name = "tw",
          path = "~/tawi-obsidian/tawa-wile/",
        },
      },
      completion = {
        nvim_cmp = true,
        min_chars = 2,
      },
      templates = {
        subdir = "_ system/templates"
      },
      attachments = {
        img_folder = "_ system/media"
      },
      daily_notes = {
        folder = "diary"
      },

      use_advanced_uri = true,
      follow_url_func = function(url)
        vim.fn.jobstart({ "xdg-open", url }) -- Open URLs in a web browser
      end,

      ui = {
        checkboxes = {
          [" "] = { char = "󰄱", hl_group = "ObsidianTodo" },
          ["x"] = { char = "", hl_group = "ObsidianDone" },
          [">"] = { char = "", hl_group = "ObsidianRightArrow" },
          ["!"] = { char = "󰰱", hl_group = "ObsidianTilde" },
          ["I"] = { char = "", hl_group = "ObsidianIdea" },
          ["0"] = { char = "󰟶", hl_group = "ObsidianSpeechBox" },
          ["1"] = { char = "󰟶", hl_group = "ObsidianSpeechBox" },
          ["2"] = { char = "󰟶", hl_group = "ObsidianSpeechBox" },
          ["3"] = { char = "󰟶", hl_group = "ObsidianSpeechBox" },
          ["4"] = { char = "󰟶", hl_group = "ObsidianSpeechBox" },
          ["5"] = { char = "󰟶", hl_group = "ObsidianSpeechBox" },
        },

        hl_groups = {
          ObsidianTodo = { bold = true, fg = "#fab387" },
          ObsidianDone = { bold = true, fg = "#89dceb" },
          ObsidianRightArrow = { bold = true, fg = "#74c7ec" },
          ObsidianIdea = { bold = true, fg = "#fab387" },
          ObsidianSpeechBox = { bold = true, fg = "#74c7ec" },
          ObsidianTilde = { bold = true, fg = "#f38ba8" },
          ObsidianBullet = { bold = true, fg = "#89dceb" },
          ObsidianRefText = { underline = true, fg = "#f2cdcd" },
          ObsidianExtLinkIcon = { fg = "#cba6f7" },
          ObsidianTag = { italic = true, fg = "#89dceb" },
          ObsidianHighlightText = { bg = "#f2cdcd" },
        },
      }
    })
    require("which-key").add({
      { "<Leader>E",  "<cmd>ObsidianTemplate<cr>",    desc = "Insert a template" },
      { "<Leader>bl", "<cmd>ObsidianBacklinks<cr>",   desc = "See backlinks" },
      { "<Leader>o",  "<cmd>ObsidianQuickSwitch<cr>", desc = "Open a zettel" },
      { "<Loader>so", "<cmd>ObsidianSearch<cr>",      desc = "Search in vault" },
      { "L",          "<cmd>ObsidianFollowLink<cr>",  desc = "Follow link" },
      { "L",          "<cmd>ObsidianLink<cr>",        desc = "Link selection",   mode = "v" },
    })
  end
}
