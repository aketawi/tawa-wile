-- hotkey manager and hints
local nlmappings = {
  mode = { "n" },
  { "<leader>/",  "<cmd>Telescope current_buffer_fuzzy_find<CR>",                                                                      desc = "Find in current buffer",      nowait = true, remap = false },
  { "<leader>F",  "<cmd>Telescope live_grep<cr>",                                                                                      desc = "Find text in files",          nowait = true, remap = false },
  { "<leader>a",  "<cmd>Alpha<cr>",                                                                                                    desc = "Alpha",                       nowait = true, remap = false },
  { "<leader>b",  "<cmd>lua require('telescope.builtin').buffers(require('telescope.themes').get_dropdown{previewer = false})<cr>",    desc = "Buffers",                     nowait = true, remap = false },
  { "<leader>c",  "<cmd>enew<CR>",                                                                                                     desc = "New Buffer",                  nowait = true, remap = false },
  { "<leader>e",  "<cmd>NvimTreeToggle<cr>",                                                                                           desc = "Filetree/Dec Treesitter",     nowait = true, remap = false },
  { "<leader>f",  "<cmd>lua require('telescope.builtin').find_files(require('telescope.themes').get_dropdown{previewer = false})<cr>", desc = "Find files",                  nowait = true, remap = false },
  { "<leader>g",  group = "Git",                                                                                                       nowait = true,                        remap = false },
  { "<leader>gR", "<cmd>lua require 'gitsigns'.reset_buffer()<cr>",                                                                    desc = "Reset Buffer",                nowait = true, remap = false },
  { "<leader>gd", "<cmd>Gitsigns diffthis HEAD<cr>",                                                                                   desc = "Diff",                        nowait = true, remap = false },
  { "<leader>ge", "<cmd>lua require 'gitsigns'.next_hunk()<cr>",                                                                       desc = "Next Hunk",                   nowait = true, remap = false },
  { "<leader>gi", "<cmd>lua require 'gitsigns'.prev_hunk()<cr>",                                                                       desc = "Prev Hunk",                   nowait = true, remap = false },
  { "<leader>gl", "<cmd>lua require 'gitsigns'.blame_line()<cr>",                                                                      desc = "Blame",                       nowait = true, remap = false },
  { "<leader>gp", "<cmd>lua require 'gitsigns'.preview_hunk()<cr>",                                                                    desc = "Preview Hunk",                nowait = true, remap = false },
  { "<leader>gr", "<cmd>lua require 'gitsigns'.reset_hunk()<cr>",                                                                      desc = "Reset Hunk",                  nowait = true, remap = false },
  { "<leader>gs", "<cmd>lua require 'gitsigns'.stage_hunk()<cr>",                                                                      desc = "Stage Hunk",                  nowait = true, remap = false },
  { "<leader>gu", "<cmd>lua require 'gitsigns'.undo_stage_hunk()<cr>",                                                                 desc = "Undo Stage Hunk",             nowait = true, remap = false },
  { "<leader>h",  "<cmd>nohlsearch<CR>",                                                                                               desc = "No Highlight",                nowait = true, remap = false },
  { "<leader>i",  group = "Local actions/Inc Treesitter",                                                                              nowait = true,                        remap = false },
  { "<leader>il", "<cmd> VisitLinkUnderCursor<cr>",                                                                                    desc = "Visit link under cursor",     nowait = true, remap = false },
  { "<leader>k",  group = "Workspace",                                                                                                 nowait = true,                        remap = false },
  { "<leader>ka", "<cmd> lua vim.lsp.buf.add_workspace_folder()<CR>",                                                                  desc = "Add workspace dir",           nowait = true, remap = false },
  { "<leader>kl", "<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders())) <CR>",                                            desc = "Current workspace",           nowait = true, remap = false },
  { "<leader>kr", "<cmd> lua vim.lsp.buf.remove_workspace_folder()<CR>",                                                               desc = "Remove workspace dir",        nowait = true, remap = false },
  { "<leader>l",  group = "LSP",                                                                                                       nowait = true,                        remap = false },
  { "<leader>lS", "<cmd>Telescope lsp_dynamic_workspace_symbols<cr>",                                                                  desc = "Workspace Symbols",           nowait = true, remap = false },
  { "<leader>la", "<cmd>lua vim.lsp.buf.code_action()<cr>",                                                                            desc = "Code Action",                 nowait = true, remap = false },
  { "<leader>ld", "<cmd>Telescope diagnostics<cr>",                                                                                    desc = "Diagnostics list",            nowait = true, remap = false },
  { "<leader>lf", "<cmd>lua lsp_formatting()<CR>",                                                                                     desc = "Format",                      nowait = true, remap = false },
  { "<leader>lg", "<cmd>lua vim.lsp.buf.signature_help()<cr>",                                                                         desc = "Signature help",              nowait = true, remap = false },
  { "<leader>li", "<cmd>LspInfo<cr>",                                                                                                  desc = "Info",                        nowait = true, remap = false },
  { "<leader>ll", "<cmd>lua vim.lsp.codelens.run()<cr>",                                                                               desc = "CodeLens Action",             nowait = true, remap = false },
  { "<leader>lp", "<cmd>lua vim.diagnostic.open_float()<cr>",                                                                          desc = "Open float",                  nowait = true, remap = false },
  { "<leader>lr", "<cmd>lua vim.lsp.buf.rename()<cr>",                                                                                 desc = "Rename",                      nowait = true, remap = false },
  { "<leader>ls", "<cmd>Telescope lsp_document_symbols<cr>",                                                                           desc = "Document Symbols",            nowait = true, remap = false },
  { "<leader>lu", "<cmd>LspStart<CR>",                                                                                                 desc = "Start LSP",                   nowait = true, remap = false },
  { "<leader>ly", "<cmd>LspStop<CR>",                                                                                                  desc = "Stop LSP",                    nowait = true, remap = false },
  { "<leader>p",  group = "Plugins, previews",                                                                                         nowait = true,                        remap = false },
  { "<leader>pg", "<cmd>Glow<cr>",                                                                                                     desc = "Preview markdown in Glow",    nowait = true, remap = false },
  { "<leader>pl", "<cmd>Lazy<cr>",                                                                                                     desc = "Lazy",                        nowait = true, remap = false },
  { "<leader>pm", "<cmd>Mason<cr>",                                                                                                    desc = "Mason",                       nowait = true, remap = false },
  { "<leader>pn", "<cmd>NullLsInfo<cr>",                                                                                               desc = "NullLS",                      nowait = true, remap = false },
  { "<leader>q",  "<cmd>q!<CR>",                                                                                                       desc = "Quit",                        nowait = true, remap = false },
  { "<leader>s",  group = "Search",                                                                                                    nowait = true,                        remap = false },
  { "<leader>sC", "<cmd>Telescope commands<cr>",                                                                                       desc = "Commands",                    nowait = true, remap = false },
  { "<leader>sR", "<cmd>Telescope registers<cr>",                                                                                      desc = "Registers",                   nowait = true, remap = false },
  { "<leader>sh", "<cmd>Telescope help_tags<cr>",                                                                                      desc = "Find Help",                   nowait = true, remap = false },
  { "<leader>sk", "<cmd>Telescope keymaps<cr>",                                                                                        desc = "Keymaps",                     nowait = true, remap = false },
  { "<leader>sr", "<cmd>Telescope oldfiles<cr>",                                                                                       desc = "Open Recent File",            nowait = true, remap = false },
  { "<leader>st", "<cmd>TodoTelescope<CR>",                                                                                            desc = "Todos",                       nowait = true, remap = false },
  { "<leader>u",  group = "UI",                                                                                                        nowait = true,                        remap = false },
  { "<leader>ua", "<cmd>lua require('noice').cmd('all')<cr>",                                                                          desc = "Show all messages",           nowait = true, remap = false },
  { "<leader>ud", "<cmd>lua require('noice').cmd('dismiss')<cr>",                                                                      desc = "Dismiss all messages",        nowait = true, remap = false },
  { "<leader>uh", "<cmd>lua if vim.api.nvim_get_option('laststatus') == 2 then vim.o.laststatus=0 else vim.o.laststatus=2 end<CR>",    desc = "Toggle Lualine",              nowait = true, remap = false },
  { "<leader>ul", "<cmd>lua require('noice').cmd('last')<cr>",                                                                         desc = "Show last message",           nowait = true, remap = false },
  { "<leader>un", "<cmd>set invnumber<cr>",                                                                                            desc = "Toggle line numbers",         nowait = true, remap = false },
  { "<leader>ur", "<cmd>set invrelativenumber<cr>",                                                                                    desc = "Toggle relative line number", nowait = true, remap = false },
  { "<leader>uw", "<cmd>set invwrap<cr>",                                                                                              desc = "Toggle word wrap",            nowait = true, remap = false },
  { "<leader>w",  "<cmd>w!<CR>",                                                                                                       desc = "Save",                        nowait = true, remap = false },
  { "<leader>x",  "<cmd>bd!<CR>",                                                                                                      desc = "Close Buffer",                nowait = true, remap = false },
  { "<leader>z",  "<cmd>ZenMode<cr>",                                                                                                  desc = "Enter Zen",                   nowait = true, remap = false },
}

local vlmappings = {
  mode = { "v" },
  { "<leader>a",  group = "align",                                                                  nowait = true,               remap = false },
  { "<leader>aa", '<cmd> lua require "align".align_to_char({length = 1})<CR>',                      desc = "to character",       nowait = true, remap = false },
  { "<leader>ad", '<cmd> lua require "align".align_to_char({length = 2, preview = true})<CR>',      desc = "to two characters",  nowait = true, remap = false },
  { "<leader>ar", '<cmd> lua require "align".align_to_string({regex = true, preview = true})<CR>',  desc = "to regex",           nowait = true, remap = false },
  { "<leader>aw", '<cmd> lua require "align".align_to_string({regex = false, preview = true})<CR>', desc = "to string",          nowait = true, remap = false },
  { "<leader>c",  group = "Comment style",                                                          nowait = true,               remap = false },
  { "<leader>cc", "<cmd>CBcbox18<CR>",                                                              desc = "Callout box",        nowait = true, remap = false },
  { "<leader>cl", "<cmd>CBllbox<CR>",                                                               desc = "Left aligned box",   nowait = true, remap = false },
  { "<leader>cq", "<cmd>CBcbox12<CR>",                                                              desc = "Quote box",          nowait = true, remap = false },
  { "<leader>cr", "<cmd>CBlrbox<CR>",                                                               desc = "Right aligned box",  nowait = true, remap = false },
  { "<leader>e",  desc = "Decrement Treesitter node",                                               nowait = true,               remap = false },
  { "<leader>i",  desc = "Increment Treesitter node",                                               nowait = true,               remap = false },
  { "<leader>l",  group = "LSP",                                                                    nowait = true,               remap = false },
  { "<leader>la", "<cmd>'<,'>lua vim.lsp.buf.range_code_action()<CR>",                              desc = "Code actions",       nowait = true, remap = false },
  { "<leader>s",  group = "Sort",                                                                   nowait = true,               remap = false },
  { "<leader>si", "<Esc><cmd>Sort i<CR>",                                                           desc = "Sort, ignore case",  nowait = true, remap = false },
  { "<leader>sr", "<Esc><cmd>Sort! <CR>",                                                           desc = "Sort, reverse",      nowait = true, remap = false },
  { "<leader>ss", "<Esc><cmd>Sort <CR>",                                                            desc = "Sort by delimiters", nowait = true, remap = false },
  { "<leader>su", "<Esc><cmd>Sort u<CR>",                                                           desc = "Sort, unique",       nowait = true, remap = false },
  { "<leader>sx", "<Esc><cmd>Sort x<CR>",                                                           desc = "Sort, hex",          nowait = true, remap = false },
}

local nmappings = {
  { "B",       "<cmd>lua require('spider').motion('ge')<CR>",       desc = "Spider-backend",       nowait = true, remap = false },
  { "S-Space", desc = "Show Rust hover",                            nowait = true,                 remap = false },
  { "C-e",     desc = "Down window",                                nowait = true,                 remap = false },
  { "C-i",     desc = "Up window",                                  nowait = true,                 remap = false },
  { "C-n",     desc = "Left window",                                nowait = true,                 remap = false },
  { "C-o",     desc = "Right window",                               nowait = true,                 remap = false },
  { "C-q",     desc = "Switch macro slot",                          nowait = true,                 remap = false },
  { "E",       "<cmd>lua require('spider').motion('e')<CR>",        desc = "Spider-end",           nowait = true, remap = false },
  { "I",       "<cmd>HopWord<CR>",                                  desc = "Hop to a word",        nowait = true, remap = false },
  { "L",       "gx",                                                desc = "Follow link",          nowait = true, remap = false },
  { "Q",       desc = "Play back macro",                            nowait = true,                 remap = false },
  { "[c",      desc = "Previous difference",                        nowait = true,                 remap = false },
  { "[d",      "<cmd>lua vim.diagnostic.goto_prev()<CR>",           desc = "Previous diagnostic",  nowait = true, remap = false },
  { "[t",      "<cmd>lua require('todo-comments').jump_prev()<CR>", desc = "Previous Todo",        nowait = true, remap = false },
  { "[x",      "<cmd>GitConflictPrevConflict<cr>",                  desc = "Previous confict",     nowait = true, remap = false },
  { "]c",      desc = "Next difference",                            nowait = true,                 remap = false },
  { "]d",      "<cmd>lua vim.diagnostic.goto_next()<CR>",           desc = "Next diagnostic",      nowait = true, remap = false },
  { "]t",      "<cmd>lua require('todo-comments').jump_next()<CR>", desc = "Next Todo",            nowait = true, remap = false },
  { "]x",      "<cmd>GitConflictNextConflict<cr>",                  desc = "Next confict",         nowait = true, remap = false },
  { "b",       "<cmd>lua require('spider').motion('b')<CR>",        desc = "Spider-back",          nowait = true, remap = false },
  { "c",       group = "Change and Conflicts",                      nowait = true,                 remap = false },
  { "c0",      desc = "choose none",                                nowait = true,                 remap = false },
  { "cac",     desc = "around class",                               nowait = true,                 remap = false },
  { "caf",     desc = "around function",                            nowait = true,                 remap = false },
  { "cai",     desc = "around conditional",                         nowait = true,                 remap = false },
  { "cb",      desc = "choose both",                                nowait = true,                 remap = false },
  { "co",      desc = "choose ours",                                nowait = true,                 remap = false },
  { "cq",      desc = "Edit macro",                                 nowait = true,                 remap = false },
  { "ct",      desc = "choose theirs",                              nowait = true,                 remap = false },
  { "cuc",     desc = "inside class",                               nowait = true,                 remap = false },
  { "cuf",     desc = "inside function",                            nowait = true,                 remap = false },
  { "cui",     desc = "inside conditional",                         nowait = true,                 remap = false },
  { "do",      desc = "Diff Obtain",                                nowait = true,                 remap = false },
  { "dp",      desc = "Diff Put",                                   nowait = true,                 remap = false },
  { "g",       group = "Go to",                                     nowait = true,                 remap = false },
  { "gD",      "<cmd>lua vim.lsp.buf.declaration()<CR>",            desc = "Go to declaration",    nowait = true, remap = false },
  { "gbc",     desc = "block comment out",                          nowait = true,                 remap = false },
  { "gcA",     desc = "add comment at end of line",                 nowait = true,                 remap = false },
  { "gcc",     desc = "comment out",                                nowait = true,                 remap = false },
  { "gd",      "<cmd>lua vim.lsp.buf.definition()<CR>",             desc = "Go to definition",     nowait = true, remap = false },
  { "gi",      "<cmd>lua vim.lsp.buf.implementation()<CR>",         desc = "Go to implementation", nowait = true, remap = false },
  { "gr",      "<cmd>lua vim.lsp.buf.references()<CR>",             desc = "Go to references",     nowait = true, remap = false },
  { "j",       "<cmd>lua vim.lsp.buf.hover()<CR>",                  desc = "Open hover",           nowait = true, remap = false },
  { "q",       desc = "Record macro",                               nowait = true,                 remap = false },
  { "ts",      desc = "Start Treesitter node selection",            nowait = true,                 remap = false },
  { "w",       "<cmd>lua require('spider').motion('w')<CR>",        desc = "Spider-word",          nowait = true, remap = false },
  { "yq",      desc = "Yank macro",                                 nowait = true,                 remap = false },
}

local vmappings = {
  mode = { "v" },
  { "I",  "<CMD>HopWord<CR>",         desc = "Hop to a word", nowait = true, remap = false },
  { "gb", desc = "block comment out", nowait = true,          remap = false },
  { "gc", desc = "comment out",       nowait = true,          remap = false },
}

setup = {
  show_help = true,         -- show help message on the command line when the popup is visible
  plugins = {
    marks = true,           -- shows a list of your marks on ' and `
    registers = true,       -- shows your registers on " in NORMAL or <C-r> in INSERT mode
    presets = {
      operators = false,    -- adds help for operators like d, y, ... and registers them for motion / text object completion
      motions = false,      -- adds help for motions
      text_objects = false, -- help for text objects triggered after entering an operator
      windows = false,      -- default bindings on <c-w>
      nav = true,           -- misc bindings to work with windows
      z = true,             -- bindings for folds, spelling and others prefixed with z
      g = true,             -- bindings for prefixed with g
    },
  },

  triggers = {
    { "<auto>", mode = "nixsotc" },
    { "a",      mode = { "n", "v" } },
  },

  icons = {
    breadcrumb = "»", -- symbol used in the command line area that shows your active key combo
    separator = "➜", -- symbol used between a key and its label
    group = "+", -- symbol prepended to a group
    mappings = false
  },

  -- window = {
  --   border = "rounded",         -- none, single, double, shadow
  --   position = "bottom",        -- bottom, top
  --   margin = { 1, 0, 1, 0 },    -- extra window margin [top, right, bottom, left]
  --   padding = { 2, 2, 2, 2 },   -- extra window padding [top, right, bottom, left]
  --   winblend = 0,
  -- },
}

return {
  "folke/which-key.nvim",
  name = "which-key",
  dependencies = { "echasnovski/mini.icons" },
  config = function()
    local whichkey = require("which-key")
    whichkey.setup(setup)
    whichkey.add(nlmappings)
    whichkey.add(vlmappings)
    whichkey.add(nmappings)
    whichkey.add(vmappings)

    vim.o.timeout = true -- time before the whichkey hint appears
    vim.o.timeoutlen = 200
  end
}
