-- beautify comments with boxes and stuff
-- binds in whichkey
return {
  "LudoPinelli/comment-box.nvim",
  opts = {
    doc_width = 80,
    box_width = 70
  }
}
