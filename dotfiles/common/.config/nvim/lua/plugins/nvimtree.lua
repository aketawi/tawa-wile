-- file explorer
local function on_attach(bufnr)
  local api = require "nvim-tree.api"

  local function opts(desc)
    return { desc = "nvim-tree: " .. desc, buffer = bufnr, noremap = true, silent = true, nowait = true }
  end

  -- load default mappings
  api.config.mappings.default_on_attach(bufnr)
  vim.keymap.set('n', "o", api.node.open.edit, opts('Open with left move'))
  vim.keymap.set('n', "E", api.node.navigate.sibling.last, opts('Last sibling'))
  vim.keymap.set('n', "I", api.node.navigate.sibling.first, opts('First sibling'))
  vim.keymap.del('n', 'e', opts("")) -- make sure colemak neio works
  vim.keymap.del('n', 'm', opts(""))
  vim.keymap.set('n', 'm', api.fs.rename_basename, opts('Rename'))

  -- fix window navigation
  vim.keymap.set("n", "<C-n>", "<C-w>w", { noremap = true, silent = true })
  vim.keymap.set("n", "<C-i>", "<C-w>w", { noremap = false, silent = true })
end

return {
  "nvim-tree/nvim-tree.lua",
  config = function()
    vim.g.nvim_tree_icons = {
      default = "",
      symlink = "",
      git = {
        unstaged = "",
        staged = "S",
        unmerged = "",
        renamed = "➜",
        deleted = "",
        untracked = "U",
        ignored = "◌",
      },
      folder = {
        default = "",
        open = "",
        empty = "",
        empty_open = "",
        symlink = "",
      },
    }

    local nvimtree = require("nvim-tree")

    nvimtree.setup({
      on_attach = on_attach,

      disable_netrw = true,
      hijack_netrw = true,

      open_on_tab = false,
      hijack_cursor = false,
      update_cwd = true,

      diagnostics = {
        enable = true,
        icons = {
          hint = "",
          info = "",
          warning = "",
          error = "",
        },
      },

      update_focused_file = {
        enable = true,
        update_cwd = true,
        ignore_list = {},
      },

      system_open = {
        cmd = nil,
        args = {},
      },

      filters = {
        dotfiles = false,
        custom = {},
      },

      git = {
        enable = true,
        ignore = true,
        timeout = 500,
      },

      view = {
        width = 30,
        side = "left",
      },

      renderer = {
        root_folder_label = false,
      },

    })
  end
}
