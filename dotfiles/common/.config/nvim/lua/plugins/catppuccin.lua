-- colorscheme of choice
return {
  "catppuccin/nvim",
  name = "catppuccin",
  priority = 1000,

  config = function()
    require("catppuccin").setup({
      flavour = "macchiato", -- latte, frappe, macchiato, mocha
      transparent_background = true,

      integrations = {
        cmp = true,
        gitsigns = true,
        nvimtree = true,
        treesitter = true,
        telescope = true,
        which_key = true,
        mason = true,
        hop = true,
        noice = true,
        notify = true,
      },

      -- better readability for comments
      custom_highlights = function(colors)
        return {
          Comment = { fg = colors.subtext0 },
          LineNr = { fg = colors.overlay1 },
        }
      end
    })

    vim.cmd.colorscheme "catppuccin"
  end
}
