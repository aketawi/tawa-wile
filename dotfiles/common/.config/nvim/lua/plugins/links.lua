return {
  "xiyaowong/link-visitor.nvim",
  config = function()
    require("link-visitor").setup({
      open_cmd = "xdg-open",
      silent = true,            -- disable all prints, `false` by defaults skip_confirmation
      skip_confirmation = true, -- Skip the confirmation step, default: false
      border = "rounded"        -- none, single, double, rounded, solid, shadow see `:h nvim_open_win()`
    })
  end
}
