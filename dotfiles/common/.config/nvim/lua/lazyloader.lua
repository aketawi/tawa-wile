local fn = vim.fn

-- Diverging from the guide and instlaling Lazy instead of Packer
local lazypath = fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
  spec = { import = "plugins" }, -- loads plugins from plugin folder
  defaults = {
    lazy = false,                -- don't lazy load plugins by default
    version = false              -- always use the latest git commit
  },
  checker = { enabled = false }, -- don't check for plugin updates
  ui = {
    border = "rounded"
  }
})

--------------------------------------------------------------------
