[[ -z $XDG_DATA_HOME ]] && XDG_DATA_HOME="$HOME"/.local/share
[[ -z $XDG_CONFIG_HOME ]] && XDG_CONFIG_HOME="$HOME"/.config
[[ -z $XDG_CACHE_HOME ]] && XDG_CACHE_HOME="$HOME"/.cache

export ANDROID_HOME="$XDG_DATA_HOME"/android
export CALCHISTFILE="$XDG_CACHE_HOME"/calc_history
export GRADLE_USER_HOME="$XDG_DATA_HOME"/gradle
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export IPYTHONDIR="$XDG_CONFIG_HOME"/ipython
export JUPYTER_CONFIG_DIR="$XDG_CONFIG_HOME"/jupyter
export TERMINFO="$XDG_DATA_HOME"/terminfo
export TERMINFO_DIRS="$XDG_DATA_HOME"/terminfo:/usr/share/terminfo
export NUGET_PACKAGES="$XDG_CACHE_HOME"/NuGetPackages
export PARALLEL_HOME="$XDG_CONFIG_HOME"/parallel
export RUSTUP_HOME="$XDG_DATA_HOME"/rustup
export CARGO_HOME="$XDG_DATA_HOME"/cargo
export SQLITE_HISTORY="$XDG_CACHE_HOME"/sqlite_history
export W3M_DIR="$XDG_CONFIG_HOME"/w3m

# Other
export GRIM_DEFAULT_DIR="$HOME"/Pictures/
export XDG_PICTURES_DIR="$HOME"/Pictures/
