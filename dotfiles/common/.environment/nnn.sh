export NNN_ARCHIVE="\\.(7z|a|ace|alz|arc|arj|bz|bz2|cab|cpio|deb|gz|jar|lha|lz|lzh|lzma|lzo|rar|rpm|rz|t7z|tar|tbz|tbz2|tgz|tlz|txz|tZ|tzo|war|xpi|xz|Z|zip)$"
export NNN_COLORS='2314'
export NNN_FIFO=/tmp/nnn.fifo
export NNN_PLUG='p:preview-tui;d:dragdrop;z:autojump;f:fzopen;k:kdeconnect;E:suedit;c:cleep;u:upload;r:rsynccp'
export NNN_TERMINAL="alacritty"
export NNN_RESTRICT_NAV_OPEN=0
export NNN_RESTRICT_0B=1
export NNN_USE_EDITOR=1
