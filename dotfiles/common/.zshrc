# source the environment, needed for other functions to work
source ~/.zsh/environment.zsh

# stop loading if not interactive
if [[ $- != *i* ]] || [[ $(tty) == "/dev/tty1" ]]; then
  return
fi

# stop loading if on tty1 (which should only be a login tty in this setup)
# [[ $(tty) == "/dev/tty1" ]] && return

# if not in ssh, pop into tmux session "user-main", create if doesn't exist
if [[ -x "$(command -v tmux)" ]] && [[ -z "${TMUX}" ]] && [[ -z "${SSH_CLIENT}" ]]; then
    tmux new -As "${USER}-main" >/dev/null 2>&1
fi

# Zsh features
setopt autocd extendedglob
autoload zmv

# Load Znap
[[ -r ~/.zshplugins/zsh-snap/znap.zsh ]] || git clone --depth 1 https://github.com/marlonrichert/zsh-snap.git ~/.zshplugins/zsh-snap/
source ~/.zshplugins/zsh-snap/znap.zsh

# Start with Starship, print plan
echo "☆.。.:*・°☆.。.:*・°☆.。.:*・°☆.。.:*・°☆"
cat ~/.plan
source <(/usr/local/bin/starship init zsh --print-full-init)
znap prompt 

# Source configurations
# re-sources environment.sh which is inefficient but whatever
for conf in $(\ls ~/.zsh/*.(z)sh); do 
  source $conf
done

# Source local configurations
[[ -d ~/.zshloc ]] && {
  for conf in $(\ls ~/.zshloc/*.(z)sh); do 
    source $conf
  done
}
