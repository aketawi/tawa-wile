function manual_resume()
    resume_time = mp.get_property_number('time-pos')
    if (resume_time > 1) then
        mp.set_property_number('time-pos', 0)
        mp.set_property_bool('pause', false)
        
        osd_duration = mp.get_property_number('osd-duration') / 1000
        mp.osd_message('Press ; to resume from last position', 2 * osd_duration)
    end
end
 
function resume_playback()
    if (resume_time > 1) then
        mp.set_property_number('time-pos', resume_time) 
    end
end
 
mp.add_key_binding(';', resume_playback)
 
mp.register_event("file-loaded", manual_resume)
