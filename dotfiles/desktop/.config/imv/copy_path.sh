#!/bin/bash
while read line; do
  if [[ $SESSION == x11 ]]; then
    echo "$line" | xsel -b -t 500 --trim
  else
    echo "$line" | wl-copy
  fi
done
