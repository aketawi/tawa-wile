-- deps
local awful = require("awful")

-- define layouts
tag.connect_signal("request::default_layouts", function()
	awful.layout.append_default_layouts({
		awful.layout.suit.spiral.dwindle,
		awful.layout.suit.tile,
		--awful.layout.suit.floating,
	})
end)

-- apply layouts
screen.connect_signal("request::desktop_decoration", function(s)
	local names = { "1", "2", "3", "4", "5" }
	local l = awful.layout.suit
	awful.tag(names, s, l.spiral.dwindle)
end)

-- Signal function to execute when a new client appears.
client.connect_signal("manage", function(c)
	-- Set the window as a slave (put it at the end of others instead of setting it as master)
	if not awesome.startup then
		awful.client.setslave(c)
	end

	if awesome.startup and not c.size_hints.user_position and not c.size_hints.program_position then
		-- Prevent clients from being unreachable after screen count changes.
		awful.placement.no_offscreen(c)
	end
end)
