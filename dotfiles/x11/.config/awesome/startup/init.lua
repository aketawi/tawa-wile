local awful = require("awful")

-- apply rules to dashboard windows, etc
require("startup.rules")

-- run startup scripts
awful.spawn.with_shell("sh ~/.scripts/x11-wm/reset-polybar.sh")
awful.spawn.with_shell("sh ~/.scripts/x11-wm/dashboard/init-dashboard.sh")
