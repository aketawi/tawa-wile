#!/bin/bash
# Houses some functions for the rest of the scripts
# Probably don't run on its own.

prompt_install() {
  unset YES
  if [[ ! $QUICK == true ]]; then
    echo -e "\033[33;1m$1\033[0m"
    [[ -z $2 ]] || echo -e "$2"
    select answer in yes no; do
      case $answer in
        yes)
          YES="yes"
          export YES
          break
          ;;
        *) break ;;
      esac
    done
  fi
}

install_packages() {
  # TODO: parse opts
  # -y = --noconfirm or -y
  # install packages by using package manager that $DISTRO uses
  return
}

export -f prompt_install
export -f install_packages
