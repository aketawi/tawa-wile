#!/bin/bash
# Installs a comfy base setup
# Do NOT run by its own, only from install.sh
# Environment things and all

# make sure we iterate over .files
shopt -s dotglob

#  ┌                                                                    ┐
#  │         Install dotfiles, prompt user if not in quick mode         │
#  └                                                                    ┘
prompt_install "Install server dotfiles?"

if [[ $QUICK == true ]] || [[ $YES == yes ]]; then
  for f in "$DOTFILES/server/"*; do
    cp -asv "$OVERWRITE" "$f" "$HOME"
  done
fi

#  ┌                                                                    ┐
#  │  Bunch of server programs, such as docker, process monitors, etc   │
#  └                                                                    ┘
prompt_install "Install server programs?"

if [[ $QUICK == true ]] || [[ $YES == yes ]]; then
  # comfy process monitor
  cargo install mprocs

  case $DISTRO in
    ubuntu)
      # install docker
      sudo apt install ca-certificates curl gnupg
      sudo install -m 0755 -d /etc/apt/keyrings
      curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
      sudo chmod a+r /etc/apt/keyrings/docker.gpg

      echo \
        "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
      $(. /etc/os-release && echo $VERSION_CODENAME) stable" |
        sudo tee /etc/apt/sources.list.d/docker.list >/dev/null

      sudo apt update
      sudo apt install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

      # install lazydocker
      curl https://raw.githubusercontent.com/jesseduffield/lazydocker/master/scripts/install_update_linux.sh | bash
      ;;

    rocky)
      # install docker, add user to docker group, add lazydocker
      sudo dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
      sudo dnf -y install docker-ce docker-ce-cli containerd.io docker-compose-plugin
      sudo systemctl enable --now docker
      sudo usermod "$USER" -aG docker
      curl https://raw.githubusercontent.com/jesseduffield/lazydocker/master/scripts/install_update_linux.sh | bash

      # install ZFS
      sudo dnf install "https://zfsonlinux.org/epel/zfs-release-2-2$(rpm --eval "%{dist}").noarch.rpm"
      sudo dnf -y install zfs
      sudo systemctl enable --now zfs-import-cache.service
      sudo systemctl enable --now zfs.target
      sudo systemctl enable --now zfs-import.target
      sudo systemctl enable --now zfs-mount.service

      # set up mail
      sudo dnf -y install exim neomutt
      sudo systemctl enable --now exim
      ;;

    *) ;;
  esac

  # logging
  sudo mkdir "/var/log/$USER"
  sudo chown $USER:$USER "/var/log/$USER"
fi
