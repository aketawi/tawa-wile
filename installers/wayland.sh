#!/bin/bash
# Installs a comfy base setup
# Do NOT run by its own, only from install.sh
# Environment things and all

# make sure we iterate over .files
shopt -s dotglob

#  ┌                                                                    ┐
#  │         Install dotfiles, prompt user if not in quick mode         │
#  └                                                                    ┘
prompt_install "Install Wayland dotfiles?"

if [[ $QUICK == true ]] || [[ $YES == yes ]]; then
  for f in "$DOTFILES/wayland/"*; do
    cp -asv $OVERWRITE $f "$HOME"
  done
fi

#  ┌                                                                    ┐
#  │      Bunch of wayland programs, mostly for the window manager      │
#  └                                                                    ┘
prompt_install "Install Wayland programs?"

if [[ $QUICK == true ]] || [[ $YES == yes ]]; then

  # sway window manager and extras
  paru -S sway swayidle waybar chayang wlogout kickoff mako notify-send --noconfirm

  # screenshot util
  paru -S slurp grim  --noconfirm
fi
