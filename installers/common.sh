#!/bin/bash
# Some configurations and apps common across all installs
# Do NOT run by its own, only from install.sh
# Environment things and all

# make sure we iterate over .files
shopt -s dotglob

#  ┌                                                                    ┐
#  │         Install dotfiles, prompt user if not in quick mode         │
#  └                                                                    ┘
prompt_install "Install common dotfiles?"

[[ $QUICK == true ]] || [[ $YES == yes ]] && {
  for f in "$DOTFILES/common/"*; do
    cp -asv "$OVERWRITE" "$f" "$HOME"
  done
  echo -e "\033[33;1mYou may want to install and run 'lesskey' to change keybinds to Colemak ones.\n\033[0m"
}

#  ┌                                                                    ┐
#  │                          Various scripts                           │
#  └                                                                    ┘
prompt_install "Install scripts to ~/.scripts/?
Some of them are needed for proper WM function"

if [[ $QUICK == true ]] || [[ $YES == yes ]]; then
  [[ -d "$HOME/.scripts" ]] || mkdir "$HOME/.scripts/"
  for f in "$DOTFILES/../scripts/"*; do
    cp -asv "$OVERWRITE" "$f" "$HOME/.scripts/"
  done
fi

#  ┌                                                                    ┐
#  │                       Rusty programs I like                        │
#  └                                                                    ┘
prompt_install "Install Rust alternatives?" "Namely:
eza, bat, sd, d-dust, fd-find, ripgrep, procs and trashy"

if [[ $QUICK == true ]] || [[ $YES == yes ]]; then
  cargo install eza     # ls
  cargo install bat     # cat
  cargo install sd      # sed
  cargo install du-dust # du
  cargo install fd-find # find
  cargo install ripgrep # grep
  cargo install procs   # ps
  cargo install trashy  # trash instead of delete, aliased to `rt`

  # other notable mentions:
  #   xh       - simple http request composer, kinda like curl
  #   tealdeer - tldr, cheatsheets and other quick docs
  #   mprocs   - TUI for running a batch of processes, good for a server
fi

#  ┌                                                                    ┐
#  │                     Some administration stuff                      │
#  └                                                                    ┘
if [[ $DISTRO == "arch" ]] || [[ $DISTRO == "artix" ]]; then
  prompt_install "Apply Arch administration configurations?" "Includes:
  - establishing a log directory for your user
  - running pacdiff after system upgrades via a pacman hook
  - running reflector weekly
  - running autotrash weekly (just for your user)
  - some pacman configs
  "

  if [[ $QUICK == true ]] || [[ $YES == yes ]]; then

    ### logging
    sudo mkdir "/var/log/$USER"
    sudo chown $USER:$USER "/var/log/$USER"

    ### pacdiff
    paru -S pacdiff-pacman-hook-git --noconfirm

    ### reflector
    sudo pacman -S reflector -noconfirm
    \cat <<EOF | sudo tee -a /etc/anacrontab >/dev/null
# update repository mirrors once a week for optimal download speed
@weekly 30 reflector reflector -f 30 -l 30 --number 10 --save /etc/pacman.d/mirrorlist
EOF

    ### autotrash
    sudo paru -S autotrash --noconfirm
    \cat <<EOF >>"/var/spool/cron/$USER"
# empty files older than 30 days every week
@weekly /usr/bin/autotrash -d 30 --stat > /var/log/tawi/autotrash.log

# update repository mirrors once a week for optimal download speed
@weekly 30 reflector reflector -f 30 -l 30 --number 10 --save /etc/pacman.d/mirrorlist
EOF

    ### pacman and make configs
    sudo sed -i 's/#Color.*/Color\nILoveCandy/' /etc/pacman.conf # color and animation
    sudo sed -i 's/#ParallelDownloads.*/ParallelDownloads = 5/' /etc/pacman.conf
    sudo sed -i "/\[multilib\]/,/Include/"'s/^#//' /etc/pacman.conf # include multilib
    sudo sed -i 's/\(MAKEFLAGS=\).*/\1"-j6"/' /etc/makepkg.conf     # 6 threads for builds
  fi
fi

#  ┌                                                                    ┐
#  │                         SSH configurations                         │
#  └                                                                    ┘
prompt_install "Apply SSH configurations?"

if [[ $QUICK == true ]] || [[ $YES == yes ]]; then
  [[ -d ~/.ssh/keys ]] || {
    mkdir -p ~/.ssh/keys
    chmod 700 ~/.ssh/keys
  }
  \cat <<EOF >>~/.ssh/config
Host *
  AddKeysToAgent yes
  ServerAliveInterval 60
EOF

  [[ -d ~/.config/systemd/user ]] || mkdir -p ~/.config/systemd/user
  \cat <<"EOF" >>~/.config/systemd/user/ssh-agent.service
[Unit]
Description=SSH key agent

[Service]
Type=simple
Environment=SSH_AUTH_SOCK=%t/ssh-agent.socket
ExecStart=/usr/bin/ssh-agent -D -a $SSH_AUTH_SOCK

[Install]
WantedBy=default.target
EOF

  systemctl --user enable --now ssh-agent
fi
