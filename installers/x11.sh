#!/bin/bash
# Installs a comfy base setup
# Do NOT run by its own, only from install.sh
# Environment things and all

# make sure we iterate over .files
shopt -s dotglob

#  ┌                                                                    ┐
#  │         Install dotfiles, prompt user if not in quick mode         │
#  └                                                                    ┘
prompt_install "Install x11 dotfiles?"

if [[ $QUICK == true ]] || [[ $YES == yes ]]; then
	for f in "$DOTFILES/x11/"*; do
		cp -asv $OVERWRITE $f "$HOME"
	done
fi

#  ┌                                                                    ┐
#  │                  All basic programs I use on X11                   │
#  └                                                                    ┘
prompt_install "Install x11 programs?"

[[ $QUICK == true ]] || [[ $YES == yes ]] &&
  if [[ $DISTRO == "arch" ]] || [[ $DISTRO == "artix" ]]; then
    echo "We will need sudo for this."
    sudo echo "Root privs gained, installing..."

    # x11 utils
    paru -S xdotool xorg-xev xbanish-git --noconfirm

    # awesomeWM and friends
    paru -S awesome-git wmutils polybar dunst picom feh rofi rofi-calc rofi-greenclip --noconfirm

    # lockscreen, idle timeout
    paru -S betterlockscreen xidlehook --noconfirm
    systemctl --user enable --now xidlehook

    # because my sleep schedule doesn't need to be any worse than it is
    paru -S redshift --noconfirm

    # screenshot util
    paru -S flameshot --noconfirm
  fi

#  ┌                                                                    ┐
#  │                            X11 configs                             │
#  └                                                                    ┘
prompt_install "Install x11 configs?" "Things like mouse and keyboard settings"

if [[ $QUICK == true ]] || [[ $YES == yes ]]; then
  echo "We will need sudo for this."
  sudo echo "Root privs gained, installing..."

  [[ -d /etc/X11/xorg.conf.d/ ]] || sudo mkdir /etc/X11/xorg.conf.d/

  # regular cp cuz we don't want to symlink root owned files
  sudo cp "$DOTFILES/../other-configs/x11/"* /etc/X11/xorg.conf.d/
  echo -e "\033[33;1mNOTICE: to apply pointer configuration, you will need to change\033[0m
Identifier \"Kensington Expert Mouse\" in \"/etc/X11/xorg.conf.d/30-pointer.conf\"
To fit your own model. List known pointers with 'xinput'.\n"
fi

#  ┌                                                                    ┐
#  │       Sddm is optional cuz who needs display managers anyway       │
#  └                                                                    ┘
prompt_install "Install SDDM?"

if [[ $YES == yes ]]; then
  paru -S sddm --noconfirm

  # copy sddm configurations
  # no symlink cuz we don't want to mess with root owned files
  sudo cp "$DOTFILES/sudo-x11/sddm/sddm.conf" /etc/
  sudo cp -r "$DOTFILES/sudo-x11/sddm/sugar-candy" /usr/share/sddm/themes
fi

#  ┌                                                                    ┐
#  │                         Installing keyd                            │
#  └                                                                    ┘
prompt_install "Install Keyd?"
if [[ $YES == yes ]]; then
  paru -S keyd --noconfirm
  sudo cp -r "$DOTFILES/sudo-x11/keyd/" /etc/
fi
