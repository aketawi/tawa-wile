#!/bin/bash
# Installs a comfy base setup
# Do NOT run by its own, only from install.sh
# Environment things and all

cd "$HOME" || exit

#  ┌                                                                    ┐
#  │                           fix keyservers                           │
#  └                                                                    ┘
[[ -d $HOME/.gnupg ]] || mkdir "$HOME/.gnupg"
echo "keyserver hkp://keyserver.ubuntu.com:80" >>"$HOME/.gnupg/gpg.conf"

#  ┌                                                                    ┐
#  │                  Update system, get base packages                  │
#  └                                                                    ┘
echo "We will need sudo for this."
sudo echo "Root privs gained, installing..."

case $DISTRO in
  artix | arch)
    sudo pacman -Syu --needed base-devel git rustup --noconfirm

    if which paru; then  
	echo "Paru already installed!"
    else
        cd /opt/ || exit
        git clone https://aur.archlinux.org/paru.git && cd paru || exit
        makepkg -si
    fi

    # Install basic apps
    paru -S zsh btop fzf nvim qalc tmux --noconfirm
    paru -S mimi --noconfirm # xdg-open replacement
    ;;

  ubuntu)
    sudo apt update && sudo apt install git
    sudo apt install btop fzf nvim zsh tmux
    [[ -d ~/.cargo/bin/ ]] ||
      curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
    ;;

  rocky)
    sudo dnf -y install epel-release gcc
    sudo dnf -y install zsh btop fzf
    [[ -d ~/.cargo/bin/ ]] ||
      curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

    # Install nvim from github binary, while it's not yet in the repo
    [[ -z "$(which nvim)" ]] && {
      curl -s -JLO https://github.com/neovim/neovim/releases/download/stable/nvim-linux64.tar.gz
      tar xzvf nvim-linux64.tar.gz && rm nvim-linux64.tar.gz

      [[ -d ~/.local/share ]] || mkdir -p ~/.local/share
      mv -f nvim-linux64 ~/.local/share/nvim

      [[ -d ~/.local/bin ]] || mkdir -p ~/.local/bin
      ln -s "$HOME/.local/share/nvim/bin/nvim" ~/.local/bin/
    }

    # tmux repo with up to date versions
    sudo dnf install http://galaxy4.net/repo/galaxy4-release-9-current.noarch.rpm
    sudo dnf -y install tmux || {
      echo -e "\033[31;1mSeems that Tmux still signs their packages with deprecated SHA-1.\033[0m"
      echo "It's a security risk, but you can try \`sudo dnf install tmux --nogpgcheck\`."
    }
    ;;

  *)
    echo -e "\033[31;1mDistro not recognized, time to amend the script.\033[0m"
    exit 1
    ;;
esac

#  ┌                                                                    ┐
#  │               Install Znap and Starship if not found               │
#  └                                                                    ┘
[[ -r $HOME/.zshplugins/zsh-snap/znap.zsh ]] ||
  git clone --depth 1 https://github.com/marlonrichert/zsh-snap.git "$HOME/.zshplugins/zsh-snap/"

[[ -r /usr/local/bin/starship ]] ||
  curl -sS https://starship.rs/install.sh | sh

#  ┌                                                                    ┐
#  │              Install Tmux plugin manager if not found              │
#  └                                                                    ┘
if [[ -d $HOME/.config/tmux/plugins/tpm/ ]]; then
  mkdir "$HOME/.config/tmux/plugins/tpm/"
  git clone --depth 1 https://github.com/tmux-plugins/tpm "$HOME/.config/tmux/plugins/tpm/"
  echo -e "\033[33;1mtpm installed, launch tmux and execute 'leader + I' to install plugins.\033[0m"
fi

#  ┌                                                                    ┐
#  │                     Install nnn file manager                       │
#  └                                                                    ┘
mkdir "$HOME/.opt/nnn/"
git clone --depth 1 https://github.com/jarun/nnn "$HOME/.opt/nnn"
pushd || exit
cd "$HOME/.opt/nnn" || exit
cp -f "$DOTFILES../other-configs/nnn.h" ./src/
eval curl -Ls https://raw.githubusercontent.com/jarun/nnn/master/plugins/getplugs
sed -i "s/\(O_NERD :\= \)./\11/" ./Makefile
make
sudo make install
sudo make install-desktop
popd || exit
cat <<EOF >> "$HOME/.config/nnn/plugins/cleep"
#!/bin/zsh

# Description: Copy the selected file's path to clipboard
SESSION=$(loginctl show-session self -p Type | cut -d= -f2)

[[ $SESSION = "wayland" ]] &&
  echo "$PWD/$nnn" | wl-copy ||
  echo "$PWD/$nnn" | xclip -selection clipboard
EOF
chmod +x "$HOME/.config/nnn/plugins/cleep"


#  ┌                                                                    ┐
#  │             Initialize some empty folders and comments             │
#  └                                                                    ┘
if [[ ! -d $HOME/.environment ]]; then
  mkdir "$HOME/.environment/"
  cat <<EOF >>"$HOME/.environment/readme"
you're safe to add more files with variables here.
changing symlinked files will be global though.
make sure your filename ends with '.zsh'
EOF

  echo "export DOTFILES=\"$DOTFILES\"" >>"$HOME"/.environment/dotfiles.sh
fi

if [[ ! -d $HOME/.autorunloc ]]; then
  mkdir "$HOME/.autorunloc/"
  cat <<EOF >>"$HOME/.autorunloc/readme"
add your autorun scripts here, this will stay local.
make sure your filename ends with '.zsh'.
EOF

fi

if [[ ! -d $HOME/.zshloc ]]; then
  mkdir "$HOME/.zshloc/"
  cat <<EOF >>"$HOME/.zshloc/readme"
add your shell configurations here, this will stay local
good place to put completions generated by programs, for example
make sure your filename ends with '.zsh'
EOF

fi

# do wish <<-EOF heredocs worked properly
# looks ugly without indents ;.;
