#!/bin/bash
# Some configurations and apps common across all installs
# Do NOT run by its own, only from install.sh
# Environment things and all

# make sure we iterate over .files
shopt -s dotglob

#  ┌                                                                    ┐
#  │         Install dotfiles, prompt user if not in quick mode         │
#  └                                                                    ┘
prompt_install "Install desktop dotfiles?"

if [[ $QUICK == true ]] || [[ $YES == yes ]]; then
  for f in "$DOTFILES/desktop/"*; do
    cp -asv "$OVERWRITE" "$f" "$HOME"
  done
fi

#  ┌                                                                    ┐
#  │          Bunch of programs for comfy daily-driver desktop          │
#  └                                                                    ┘
prompt_install "Bootstrap desktop programs?" "(Assuming you're running Arch.)"

if [[ $QUICK == true ]] || [[ $YES == yes ]]; then
  echo "We will need sudo for this."
  sudo echo "Root privs gained, installing..."

  # browser of choice and a cheeky symlink
  paru -S waterfox-g-bin --noconfirm
  sudo ln -s /usr/bin/waterfox-g /usr/bin/firefox ||
    sudo ln -s /usr/bin/waterfox /usr/bin/firefox

  # fix for some playback issues
  paru -S ffmpeg4.4 --noconfirm

  # more rusty programs
  cargo install gitui

  # QT-less replacer for KDE's dragon
  paru -S dragon-drop --noconfirm

  # Still the best image and video players
  paru -S imv mpv --noconfirm

  # Cheatsheets and reference
  paru -S cht.sh-git --noconfirm

  # Protect from out-of-memory hang
  paru -S earlyoom --noconfirm
  sudo systemctl enable --now earlyoom

  # Bypass censorship, make your ISP spy on you less
  # and probably sell your data to Cloudflare.
  # Sometimes you even get better speed.
  # paru -S warp-cli
  # warp-cli register
  # warp-cli set-custom-endpoint 162.159.193.8:2408

  # TODO: kde-connect or some alternative
  #
  # Not adding it right now, cuz I don't want to pull
  # the entire KDE dependency hell automatically.
  #
  # Will research a way to get it working with minimal
  # dependencies before adding.
fi
