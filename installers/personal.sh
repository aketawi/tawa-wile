#!/bin/bash
# Installs a comfy base setup
# Do NOT run by its own, only from install.sh
# Environment things and all

# make sure we iterate over .files
shopt -s dotglob

#  ┌                                                                    ┐
#  │         Install dotfiles, prompt user if not in quick mode         │
#  └                                                                    ┘
prompt_install "Install personal dotfiles?"

if [[ $QUICK == true ]] || [[ $YES == yes ]]; then
  for f in "$DOTFILES/personal/"*; do
    cp -asv "$OVERWRITE" "$f" "$HOME"
  done
fi

#  ┌                                                                    ┐
#  │         Personal programs for calendars, note taking, etc.         │
#  └                                                                    ┘
prompt_install "Install personal programs?"

if [[ $QUICK == true ]] || [[ $YES == yes ]]; then

  # calendar and sync
  paru -S khal vdirsyncer --noconfirm

  # note taking
  [[ $DISTRO == artix ]] && {
    echo "Artix repostiories don't have Obsidian."
    echo "Get the appimage from https://obsidian.md/download"
  } || paru -S obsidian --noconfirm
fi
