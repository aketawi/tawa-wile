#!/bin/bash
if [[ $(dunstctl is-paused) = false ]] ; then
  dunstctl set-paused true
  echo ""
else
  dunstctl set-paused false;
  echo ""
fi
