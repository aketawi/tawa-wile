#!/bin/bash
warp_status=$(warp-cli status | cut -d ':' -f 2)

[[ $warp_status == *Connected* ]] &&
  warp-cli disconnect ||
  warp-cli connect
