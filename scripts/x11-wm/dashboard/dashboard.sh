#!/bin/bash

TERM=alacritty

# Spawns a dashboard-esque setup running in a bunch of terminals
#
# In case of issues with more instances spawning on top of old ones, run the script on its own,
# the numbers it spits are the pids of windows it matched in the if statement below.
# You want there to be as many of them as there are terminal windows spawning.
# Number one cause of issues are the quotes -> ' "
#
# The window.dimensions fields are configured for my exact dimensions and a 2560x1440 screen
# you may want to play around with them for your system
#
# "-o" settings set the terminal size so that it
# doesn't suddenly grow. awesome is a bit jank about that lol
#
# you'll want to play around and configure that for your
# own monitor, if it's not 2560x1440.
#
# roughly drag out a floating terminal window to the size
# you want it in, and run "echo `tput cols` \| `tput lines`"
# to learn the internal terminal dimensions.
#
# This probably implies you're running alacritty

run() {
  if ! pgrep -f -u $USER "$1 $2"; then
    echo "$2" | xargs "$1" &
  fi
}

# constellation background thing
if ! pgrep -f "constellation_bgr"; then
  "$HOME/code/rust/constellation_bgr/target/release/constellation_bgr" &
fi

# btop
run "$TERM" "--class startup-btop -e btop"
sleep 0.05 # in hope of Awesome putting them in correct order
# also makes a nice animation
# calendar
run "$TERM" "--class startup-cal -o window.dimensions.lines=29 -o window.dimensions.columns=94 -e $HOME/.scripts/x11-wm/dashboard/print-cal.sh"
sleep 0.05

# rss scrobbler
run "$TERM" "--class startup-rss -o window.dimensions.lines=22 -o window.dimensions.columns=57 -e newsboat -r"
sleep 0.05

# pconf window
run "$TERM" "--class startup-fetch -e $HOME/.scripts/x11-wm/dashboard/print-cat.sh"
sleep 0.05

# mechanicus quote window. set padding for extra style
run "$TERM" "--class startup-quote -e $HOME/.scripts/x11-wm/dashboard/print-quote.sh"
sleep 0.05

# system journal
run "$TERM" "--class startup-jrnl -o window.dimensions.lines=22 -o window.dimensions.columns=94 -e $HOME/.scripts/x11-wm/dashboard/print-journal.sh"
