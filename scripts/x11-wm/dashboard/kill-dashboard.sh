#!/bin/bash

TERM=alacritty
PREFIX=startup

# Cycles over every terminal window and matches the X window prefix
# as defined in dashboard.sh

for process in $(pgrep $TERM); do
	window_id=$(xdotool search --pid $process)
	window_class=$(xprop -id $window_id | grep WM_CLASS)
	echo $window_class
		if [[ $window_class = *$PREFIX* ]]; then
			kill $process
    fi
done

pkill -f "constellation_bgr"
