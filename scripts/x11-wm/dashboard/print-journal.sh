#!/bin/sh

# hide terminal cursor, very handy
tput civis

while true; do
	# shows all journalctl messages of priority level
	# above 3 (error) since this boot,
	# and omits headers and hostname, and pipes into ccze
	#
	# The whole loop thing is just so that it doesn't
	# exit immediately after printing. Hacky, but works.

	journalctl -b 0 -p 3 -qfe --output=cat --output-fields=_EXE,MESSAGE,SYSLOG_TIMESTAMP --no-hostname & # | ccze -A &
	sleep 60m
	kill $! # prevent memory leak
	clear
done
