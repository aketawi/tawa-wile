#!/bin/bash

export PF_INFO="ascii title os kernel pkgs"
export PF_ASCII="Catppuccin"

# hide terminal cursor, very handy
tput civis

while true;
do echo && 
  # run pfetch every 10 minutes so that it updates the 
  # packages, also strip the last line to keep it centered
  pfetch | head -n -2 &
  sleep 10m &&
  clear;
done
