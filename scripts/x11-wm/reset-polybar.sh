#!/bin/bash

# restart polybar
killall -q polybar

sleep 0.05 # without this, sometimes one or more bars won't spawn
# polybar left &
# polybar right &
# polybar middle &
polybar main &
