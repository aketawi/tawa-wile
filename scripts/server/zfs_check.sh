#!/bin/bash

# A utility that will regularly check the status of these zpools
zpools=(pool1 pool2)

# In the case that a pool is marked anything other than HEALTHY,
# a mail will be sent to this user:
administrator=tawi

# It is recommended to run this script via root's (ana)crontab,
# and log its output to /var/log/<admin>/smart_check.log (or wherever)
# i.e.:
# @weekly 60 schk /path/to/smart_check.sh 2>&1 >/var/log/admin/smart_check.log

[[ $UID -gt 0 ]] && {
  echo "Not running as root, so this will not behave as intended. Continue? [y/N]"
  read confirm
  [[ $confirm == 'y' ]] || exit
}

[[ $(systemctl status zed) ]] &&
  echo -e "\033[32;1mZFS Event Daemon is running alright.\033[0m" ||
  echo -e "\033[31;1mZFS Event Daemon isn't running. Check with 'systemctl status zed'.\033[0m"

for pool in ${zpools[@]}; do
  status=$(zpool status $pool)
  pool=$(echo "$status" | grep "pool:" | cut -d: -f2)
  state=$(echo "$status" | grep "state:" | cut -d: -f2)

  echo $pool: $state

  [[ "$state" = " ONLINE" ]] || {
    echo -e "\033[31;1mWARNING: this pool isn't marked as ONLINE."

    wall <<EOF
WARNING: "zpool ${pool}" is in an UNHEALTHY state.
Administrator should check their mail.
EOF

    sendmail $administrator@localhost <<EOF
Subject: UNHEALTHY state for zpool $pool

Pool "${pool}" isn't marked as ONLINE. 
It's current state is "${state}".

The full message was:
"$status"

Check via 'zpool status -v ${pool}' 

More logs may be available at /var/log/$administrator/zfs_check.log
EOF

    echo "mail sent"
  }

done
