#!/bin/bash

# Script that will back up these specified directories (or files):
dirs=(\
  "/pool1/documents" \
  "/pool1/sync" \
  "/home/volumes" \
  "/home/tawi/compose" \
)  # don't forget that you need a space after each entry  

# Or directories from standard input, if present,
# To this configured rsyncd destination:
destination=backup

# The user that owns the destination share:
owner=tawi

# Usage example with anacron:
# 1 5 rsyncbackup /path/to/backup.sh /home/user/dir1 /dir2 /dir3/subdir
# Add `2>&1 >/var/log/tawi/backup.log` at the end to also log it.

# Pay attention to trailing slash: having one will back up the
# files inside them, as if you used `dir/*`.

# Use -v to enable rsync's verbose output
while getopts "v" option; do
  case $option in
    v) VERBOSE_OUTPUT=v ;;
    *) ;;
  esac
done

[[ "$#" -gt 0 ]] && {
  echo "Using arguments as input..."
  unset dirs
  dirs=$@
}

for dir in "${dirs[@]}"; do 
  rsync -az$VERBOSE_OUTPUT "$dir" "rsync://$owner@localhost/$destination/"
done
