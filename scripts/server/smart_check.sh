#!/bin/bash

# A utility that will regularly check and run a test against these drives:
drives=(sda sdb sdc sdd sde sdf sdg sdh sdi sdj)

# Using this smartmontools method:       (short, long, conveyance, select)
method=long

# Pass -f to force a SMART check.
# In seconds, this is how often this script will run the actual testing part:
period=2628000 # 1 month

# In the case that a drive fails a SMART check, a mail will be sent to this user:
administrator=tawi

# It is recommended to run this script via root's (ana)crontab,
# and log its output to /var/log/<admin>/smart_check.log (or wherever)
# i.e.:
# @weekly 60 schk /path/to/smart_check.sh 2>&1 >/var/log/admin/smart_check.log

while getopts "f" option; do
  case $option in
    f) FORCE_CHECK=true ;;
    *) ;;
  esac
done

[[ $UID -gt 0 ]] && {
  echo "Not running as root, so this will not behave as intended. Continue? [y/N]"
  read confirm
  [[ $confirm == 'y' ]] || exit
}

[[ $(systemctl status smartd) ]] &&
  echo -e "\033[32;1mSmartd is running alright.\033[0m" ||
  echo -e "\033[31;1mSmartd isn't running. Check with 'systemctl status smartd'.\033[0m"

for drive in ${drives[@]}; do
  echo -e "\n========================================"
  echo -e "Examining \033[32;1m/dev/${drive}\033[0m..."

  specs=$(smartctl -i /dev/$drive | grep -e "Device Model" -e "Serial Number")
  echo "$specs"

  health="$(smartctl -H /dev/$drive)"
  echo "$health" | tail -n $(echo "$health" | grep -m1 -n "START OF READ" | cut -d : -f 1)

  [[ $(echo "$health" | grep "self-assessment") == *"PASSED"* ]] || {
    echo -e "\033[31;1mWARNING: this drive has failed SMART self-assessment."

    wall <<EOF
WARNING: "/dev/${drive}" has failed SMART self-assessment.
Administrator should check their mail.
EOF

    sendmail $administrator@localhost <<EOF
Subject: SMARTcheck.sh failure for /dev/$drive

Drive "/dev/${drive}" has failed a SMART test.

The drives specifications are:
"$specs"

And the failure message was:
"$health"

Check via 'smartctl -l error /dev/${drive}' 
and smartctl -l selftest /dev/${drive}.

More logs may be available at /var/log/$administrator/smart_check.log
EOF

    SKIP_TESTS=true
  }

  errlog="$(smartctl -l error /dev/$drive)"
  echo "$errlog" | tail -n $(echo "$errlog" | grep -m1 -n "START OF READ" | cut -d : -f 1)

  testlog="$(smartctl -l selftest /dev/$drive)"
  echo "$testlog" | tail -n $(echo "$testlog" | grep -m1 -n "START OF READ" | cut -d : -f 1)

  echo -e "\033[0m" # reset red tint from the warning message

  if [[ $SKIP_TESTS ]] && [[ -z $FORCE_CHECK ]]; then continue; fi
  [[ -d /var/spool/smartcheck ]] || mkdir /var/spool/smartcheck

  lr="/var/spool/smartcheck/$drive.lastrun"
  [[ -f $lr ]] || {
    echo -e "\033[33;1m$lr doesn't exist yet, creating and running tests...\033[0m"
    smartctl -t $method /dev/$drive
    date +%s >$lr
    continue
  }

  #      epoch now   >  (epoch last run + period var)
  if [[ $(date +%s) -gt $(($(cat "$lr") + $period)) ]] || [[ $FORCE_CHECK ]]; then
    echo -e "\033[33;1mLast checked more than the specified period or forcing, running/var tests...\033[0m"
    smartctl -t $method /dev/$drive
    date +%s >$lr
  else
    echo -e "\033[33;1mTests for this drive were last run at $(date -d @$(cat $lr)). \033[0m"
    echo "Next test expected to run at around $(date -d @$(($(cat $lr) + $period)))" # bash math is so jank lol
  fi

done
