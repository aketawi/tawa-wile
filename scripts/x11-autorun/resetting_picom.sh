#!/bin/env bash
set -euo pipefail

while true; do
  sleep 0.1
  picom &
  PID=$!
  sleep 7200
  kill "$PID"
done
