#!/bin/bash

# select random pictures from a directory and set as wallpapers if folder exists
[[ -d $HOME/.pretty/wallpapers ]] &&
  feh --bg-fill --no-fehbg --randomize ~/.pretty/wallpapers &
