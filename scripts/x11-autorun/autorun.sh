#!/bin/bash

# checks if the process is currently running
# in case the script is called more than once
run() {
  if ! pgrep -f "$1"; then
    $@ &
  fi
}

# run "picom"
run "redshift"
run "dunst"
run "greenclip daemon"
run "flameshot"
run "xbanish"
