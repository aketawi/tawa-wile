#!/bin/sh

# set random lockscreen wallpaper if the folder exists
# actually calling the lockscreen is handled by xidlehook service
[[ -d $HOME/.pretty/lockscreens ]] &&
  betterlockscreen -u ~/.pretty/lockscreens &
