#!/bin/env bash
set -euo pipefail

# checks if the process is currently running
# in case the script is called more than once
run() {
  if ! pgrep -f "$1"; then
    $@ &
  fi
}

run "waybar"
