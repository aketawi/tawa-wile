#!/bin/env bash
set -eu

now=$(date +%s)
$*
aft=$(date +%s)
diff=$((aft - now))
echo "Execution took "$diff" seconds."
