#!/bin/bash
# Renames subtitles with a two character long language tag
# at the end to remove it. Useful for mpv.
# e.g.: subtitle_en.srt => subtitle.srt
#
# Preview by default, pass "rename" as second argument to actually rename.
# Can step 1 level down into directories.
# Dir in argument **requires** trailing slash

# idea: check for a similar name with a video extension, rename to match

# pass "./" for pwd
if [[ $# -eq 0 ]]; then
	echo "No arguments. Exiting"
	exit
fi

# push this into its own variable for use in do_rename()
preview=$2

# define arrays to sort through later
declare dirs
declare files

do_rename() {
	file="$1"

	# todo if need arises: store subtitle extensions in array,
	# check files against it instead of hardcoding

	# if file is not a subtitle, exit
	if [[ "$file" != *".srt" ]] && [[ "$file" != *".ass" ]] && [[ "$file" != *".vtt" ]]; then
		# echo "$file is not a subtitle"
		return 0
	fi

	# bit of a messy syntax, but it needs to handle random ass underscores
	# and not touch files that don't have "_en" or other language code
	new=$(echo "$file" | perl -pe "s/(.*?)_..(?=\..*)/\$1/")

	# if filename doesn't need changing, exit
	if [[ "$file" = "$new" ]]; then
		# echo "$file doesn't need changing"
		return 0
	fi

	# Preview by default, require an additional argument to actually rename
	if [[ $preview != "rename" ]]; then
		echo "old name: $file"
		echo "new name: $new"

	else
		mv "$file" "$new"
	fi
}

############################################################

# todo if need arises:
# pop out into a while loop and recursively walk through dirs

# check file type and store in array
for file in "$1"*; do

	if [[ -d "$file" ]]; then
		dirs+=("$file")

	elif [[ -f "$file" ]]; then
		files+=("$file")

	fi
done

# rename files at pwd
for file in "${files[@]}"; do
	do_rename "$file"
done

# walk through dirs
for dir in "${dirs[@]}"; do
	for file in "$dir/"*; do
		do_rename "$file"
	done
done
