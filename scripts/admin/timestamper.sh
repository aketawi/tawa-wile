#!/bin/bash

# Appends specified timestamp to given lines
# Useful for logging

# argument for `date +`
TIME_FORMAT="[%b %d, %H:%M:%S]: "

# read all lines, append timestamp to each one
while read -r LINE; do
  echo $(date +"$TIME_FORMAT") "$LINE"
done </dev/stdin
