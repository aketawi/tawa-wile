#!/bin/bash

# pop into functions.zsh?

for i in $(pacman -Qqe); do
  grep "\[ALPM\] installed $i" /var/log/pacman.log
done |
  sort -u |
  sed -e 's/\[ALPM\] installed //' -e 's/(.*$//' |
  awk '{print $2}' | tac | uniq -u | bat
