#!/bin/sh

temp=$(sensors | grep "junction:" | tr -d '+' | awk '{print $2}')
echo "$temp"
