# mount/unmount den
alias 'mden'='sudo mount /home/files/den'
alias 'uden'='sh -c "sudo umount /home/files/den"'
# Stash a link
alias 'n'='nvim ~/tawi-obsidian/tawa-wile/_hints/links.md'
# change background
alias 'chbg'='~/.scripts/autorun/background.sh'
