#!/bin/bash
# The main script that will set up and run the installers

#  ┌                                                                    ┐
#  │                      Check if running as root                      │
#  └                                                                    ┘
if [[ $UID == 0 ]]; then
  echo -e "\033[31;1mFor the love of god do NOT run this as root.\033[0m"
  exit 1
fi

#  ┌                                                                    ┐
#  │                            Set options                             │
#  └                                                                    ┘
DOTFILES="$(pwd)/dotfiles"
DISTRO=$(awk -F '=' 'NR==3 {print $2}' </etc/os-release | sed 's/\"//g')
export DOTFILES
export DISTRO
export PS3="> "

# Don't overwrite by default
export OVERWRITE="-n"

# source functions
source ./installers/_functions.sh

#  ┌                                                                    ┐
#  │                          Parse arguments                           │
#  └                                                                    ┘
# TODO: arguments for quickly selecting layers
while getopts "qo" option; do
  case $option in
    q) export QUICK=true ;;
    o)
      echo -e "\n\033[1;33mOperating in OVERWRITE mode. Stay safe.\033[0m"
      export OVERWRITE="--remove-destination"
      ;;
    *) ;;
  esac
done

# if no arguments were given
if [[ $OPTIND -eq 1 ]]; then
  echo -e '\nYou can pass -q to skip base setup and confirmations'
  echo 'and -o to overwrite existing files'
fi

#  ┌                                                                    ┐
#  │                            Start setup                             │
#  └                                                                    ┘
if [[ ! $QUICK == true ]]; then
  echo "╭───────────────────────────────────╮"
  echo "│ welcome to the dotfiles installer │"
  echo "│                                   │"
  echo "│       perform base setup?         │"
  echo "╰───────────────────────────────────╯"

  select answer in yes no; do
    case $answer in
      yes)
        exec ./installers/base.sh
        break
        ;;
      *) break ;;
    esac
  done
fi

if [[ ! $QUICK == true ]]; then
  echo "╭───────────────────────────────────╮"
  echo "│  create a backup of ~/.config/ ?  │"
  echo "╰───────────────────────────────────╯"

  select answer in yes no; do
    case $answer in
      yes)
        cp -r "$HOME"/.config/ "$HOME"/.config.bak/
        break
        ;;
      *) break ;;
    esac
  done
fi

echo "╭───────────────────────────────────╮"
echo "│ please select a layer to install  │"
echo "╰───────────────────────────────────╯"

select layer in common desktop personal server wayland x11 quit; do
  case $layer in
    common) bash ./installers/common.sh ;;
    desktop) bash ./installers/desktop.sh ;;
    personal) bash ./installers/personal.sh ;;
    server) bash ./installers/server.sh ;;
    wayland) bash ./installers/wayland.sh ;;
    x11) bash ./installers/x11.sh ;;
    quit) exit ;;
  esac
  exit
done
